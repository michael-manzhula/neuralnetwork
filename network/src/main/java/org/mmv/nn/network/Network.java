/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.mmv.nn.network.exception.ConfigurationException;
import org.mmv.nn.network.exception.NetworkException;
import org.mmv.nn.network.node.BinaryInput;
import org.mmv.nn.network.node.BinaryOutput;
import org.mmv.nn.network.node.Input;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.node.Neuron;
import org.mmv.nn.network.node.Node;
import org.mmv.nn.network.node.NumberInput;
import org.mmv.nn.network.node.NumberOutput;
import org.mmv.nn.network.node.Output;
import org.mmv.nn.network.result.Result;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Network implements Serializable {

    /**
     * Bad solution... Introduced to support export to file big networks
     * (serialization and circular pointers)
     */
    private final Map<String, Node> nodeStorage = new HashMap<>();

    private static final long serialVersionUID = 1L;

    private int id;
    private NetworkType networkType;
    private int inputNumber;
    private int hiddenLayersNumber;
    private float learningRate;
    private String[] inputIds;
    private String[][] neuronIds;
    private String[] outputIds;

    public Network(int id) {
        // For converting form networkEntity
        this.id = id;
    }

    public Network(int inputNumber, int outputNumber, int hiddenLayersNumber, float learningRate, NetworkType networkType, NetworkEventHandler<String> messageHandler) throws ConfigurationException {
        if (networkType == null) {
            throw new ConfigurationException("Network type is null!");
        }
        this.networkType = networkType;
        this.inputNumber = inputNumber;
        this.hiddenLayersNumber = hiddenLayersNumber;
        this.learningRate = learningRate;
        this.inputIds = new String[inputNumber];
        this.neuronIds = new String[hiddenLayersNumber][inputNumber];
        this.outputIds = new String[outputNumber];
        buildNetwork(messageHandler);
    }

    private void buildNetwork(NetworkEventHandler<String> messageHandler) {
        // Create nodes
        messageHandler.handle("Creating nodes...");
        for (int i = 0; i < inputNumber; i++) {
            inputIds[i] = createInput(i);
        }
        for (int z = 0; z < hiddenLayersNumber; z++) {
            for (int i = 0; i < inputNumber; i++) {
                Neuron neuron = new Neuron(nodeStorage);
                neuronIds[z][i] = neuron.getId();
                nodeStorage.put(neuron.getId(), neuron);
            }
        }
        // Create outputs
        for (int i = 0; i < outputIds.length; i++) {
            outputIds[i] = createOutput(i);
        }
        // Connect nodes
        for (int z = 0; z < hiddenLayersNumber; z++) {
            messageHandler.handle("Connecting layer: " + (z + 1));
            String[] neuronLayer = neuronIds[z];
            if (z == 0) {
                // First neuron layer after inputs
                connectNeuronLayerWithInputs(neuronLayer);
            }
            if (z + 1 < hiddenLayersNumber) {
                // Next neuron layer exists
                String[] nextNeuronLayer = neuronIds[z + 1];
                connectNodeLayers(neuronLayer, nextNeuronLayer);
            } else {
                // Last neuron layer
                connectNodeLayers(neuronLayer, outputIds);
            }
        }
    }

    private String createInput(int order) {
        if (NetworkType.BINARY == networkType) {
            BinaryInput binaryInput = new BinaryInput();
            binaryInput.setInputOrder(order);
            nodeStorage.put(binaryInput.getId(), binaryInput);
            return binaryInput.getId();
        } else if (NetworkType.NUMBER == networkType || NetworkType.XO == networkType) {
            NumberInput numberInput = new NumberInput();
            numberInput.setInputOrder(order);
            nodeStorage.put(numberInput.getId(), numberInput);
            return numberInput.getId();
        }
        return null;
    }

    private String createOutput(int order) {
        if (NetworkType.BINARY == networkType) {
            BinaryOutput binaryOutput = new BinaryOutput(nodeStorage);
            binaryOutput.setOutputOrder(order);
            nodeStorage.put(binaryOutput.getId(), binaryOutput);
            return binaryOutput.getId();
        } else if (NetworkType.NUMBER == networkType || NetworkType.XO == networkType) {
            NumberOutput numberOutput = new NumberOutput(nodeStorage);
            numberOutput.setOutputOrder(order);
            nodeStorage.put(numberOutput.getId(), numberOutput);
            return numberOutput.getId();
        }
        return null;
    }

    private void connectNeuronLayerWithInputs(String[] neurons) {
        for (String inputId : inputIds) {
            for (String neuronId : neurons) {
                Neuron neuron = (Neuron) nodeStorage.get(neuronId);
                neuron.addInputConnection(inputId);
            }
        }
    }

    private void connectNodeLayers(String[] layerA, String[] layerB) {
        for (String neuronAId : layerA) {
            for (String neuronBId : layerB) {
                Neuron neuronA = (Neuron) nodeStorage.get(neuronAId);
                Neuron neuronB = (Neuron) nodeStorage.get(neuronBId);
                neuronA.addOutputConnection(neuronBId);
                neuronB.addInputConnection(neuronAId);
            }
        }
    }

    /**
     * Method for number network type.
     *
     * @param inputData
     *            integer input data
     * @return
     * @throws NetworkException
     */
    public void analyze(Float[] inputData, NetworkEventHandler<Result<Float>[]> handler) {
        if (!isNumberNetworkValid(inputData)) {
            handler.handle(null);
            return;
        }
        Result<Float>[] result = analyzeSync(inputData);
        handler.handle(result);
    }

    public Result<Float>[] analyzeSync(Float[] inputData) {
        if (!isNumberNetworkValid(inputData)) {
            return null;
        }
        for (String inputId : inputIds) {
            Input input = (Input) nodeStorage.get(inputId);
            Integer order = input.getInputOrder();
            Float inputValue = inputData[order];
            input.setInputValue(inputValue);
            input.calculateOutputValue();
        }
        process();
        List<Result<Float>> result = new ArrayList<>(outputIds.length);
        for (String outputId : outputIds) {
            Output output = (Output) nodeStorage.get(outputId);
            float outputValue = output.getOutputValue();
            result.add(new Result<>(outputValue, outputValue, output.getOutputOrder()));
        }
        return result.toArray(new Result[result.size()]);
    }
    
    private boolean isNumberNetworkValid(Float[] inputData) {
        return inputNumber == inputData.length && (NetworkType.NUMBER == networkType || NetworkType.XO == networkType);
    }

    /**
     * Method for binary network type.
     *
     * @param inputData
     *            binary input data
     * @return
     * @throws NetworkException
     */
    public void analyze(Boolean[] inputData, NetworkEventHandler<Result<Boolean>> handler) {
        if (inputNumber != inputData.length || NetworkType.BINARY != networkType) {
            handler.handle(null);
            return;
        }
        for (String inputId : inputIds) {
            Input input = (Input) nodeStorage.get(inputId);
            Integer order = input.getInputOrder();
            boolean inputValue = inputData[order];
            input.setInputValue(inputValue);
            input.calculateOutputValue();
        }
        process();
        String outputId = outputIds[0];
        Output<Boolean> output = (Output) nodeStorage.get(outputId);
        float outputValue = output.getOutputValue();
        handler.handle(new Result<>(output.getOutput(), outputValue, null));
    }

    private void process() {
        for (String[] neuronLayer : neuronIds) {
            for (String neuronId : neuronLayer) {
                Neuron neuron = (Neuron) nodeStorage.get(neuronId);
                neuron.calculateOutputValue();
            }
        }
        for (String outputId : outputIds) {
            Output output = (Output) nodeStorage.get(outputId);
            output.calculateOutputValue();
        }
    }

    /**
     * For number network
     * 
     * @param expectedResult
     */
    public void learn(int expectedResult) {
        for (String outputId : outputIds) {
            Output output = (Output) nodeStorage.get(outputId);
            float expectedValue = output.getOutputOrder() == expectedResult ? 1f : 0f;
            float outputValue = output.getOutputValue();
            float outputError = (expectedValue - outputValue) * outputValue * (1 - outputValue);
            output.setErrorValue(outputError);
            correctWeights(output);
        }
        learn();
    }

    /**
     * For XO network
     */
    public void learn(int cellNumber, boolean isCorrect) {
        for (String outputId : outputIds) {
            Output output = (Output) nodeStorage.get(outputId);
            if (output.getOutputOrder() == cellNumber) {
                float expectedValue = isCorrect ? 1 : 0;
                float outputValue = output.getOutputValue();
                float outputError = (expectedValue - outputValue) * outputValue * (1 - outputValue);
                output.setErrorValue(outputError);
                correctWeights(output);
                break;
            }
        }
        learn();
    }

    /**
     * For binary network
     * 
     * @param expectedResult
     */
    public void learn(boolean expectedResult) {
        float expectedValue = expectedResult ? 1f : 0f;
        String outputId = outputIds[0];
        Output output = (Output) nodeStorage.get(outputId);
        float outputValue = output.getOutputValue();
        float outputError = (expectedValue - outputValue) * outputValue * (1 - outputValue);
        output.setErrorValue(outputError);
        correctWeights(output);
        learn();
    }

    private void learn() {
        for (int z = neuronIds.length - 1; z >= 0; z--) {
            String[] neuronLayer = neuronIds[z];
            for (String neuronId : neuronLayer) {
                Neuron neuron = (Neuron) nodeStorage.get(neuronId);
                float neuronOutput = neuron.getOutputValue();
                float neuronError = 0f;
                for (String nodeId : neuron.getOutputs()) {
                    Neuron node = (Neuron) nodeStorage.get(nodeId);
                    neuronError += node.getErrorValue() * node.getInputs().get(neuronId);
                }
                neuronError *= neuronOutput * (1 - neuronOutput);
                neuron.setErrorValue(neuronError);
                correctWeights(neuron);
            }
        }
    }

    private void correctWeights(Neuron neuron) {
        float neuronError = neuron.getErrorValue();
        for (Map.Entry<String, Float> entry : neuron.getInputs().entrySet()) {
            String inputId = entry.getKey();
            Node input = nodeStorage.get(inputId);
            float inputWeight = entry.getValue();
            float inputOutputValue = input.getOutputValue();
            float weightDelta = inputOutputValue * neuronError * learningRate;
            float correctedWeight = inputWeight + weightDelta;
            entry.setValue(correctedWeight);
        }
    }

    public int getNodeCount() {
        int nodeCount = inputIds.length;
        for (String[] neuronLayer : neuronIds) {
            nodeCount += neuronLayer.length;
        }
        nodeCount += outputIds.length;
        return nodeCount;
    }

    public NetworkType getNetworkType() {
        return this.networkType;
    }

    public int getInputNumber() {
        return this.inputNumber;
    }

    public float getLearningRate() {
        return this.learningRate;
    }

    public Map<String, Node> getNodeStorage() {
        return nodeStorage;
    }

    public int getHiddenLayersNumber() {
        return hiddenLayersNumber;
    }

    public String[] getInputIds() {
        return inputIds;
    }

    public String[][] getNeuronIds() {
        return neuronIds;
    }

    public String[] getOutputIds() {
        return outputIds;
    }

    public void setNetworkType(NetworkType networkType) {
        this.networkType = networkType;
    }

    public void setLearningRate(float learningRate) {
        this.learningRate = learningRate;
    }

    public void setInputIds(String[] inputIds) {
        this.inputIds = inputIds;
        if (inputIds != null) {
            this.inputNumber = inputIds.length;
        }
    }

    public void setNeuronIds(String[][] neuronIds) {
        this.neuronIds = neuronIds;
        if (neuronIds != null) {
            this.hiddenLayersNumber = neuronIds.length;
        }
    }

    public void setOutputIds(String[] outputIds) {
        this.outputIds = outputIds;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean equalsNetwork(Network network) {
        if (network == null) {
            return false;
        }
        if (this == network) {
            return true;
        }
        if (id != network.getId() || networkType != network.networkType || inputNumber != network.inputNumber || hiddenLayersNumber != network.hiddenLayersNumber || learningRate != network.learningRate
                || nodeStorage.size() != network.nodeStorage.size()) {
            return false;
        }
        if (!compareIds(inputIds, network.inputIds)) {
            return false;
        }
        if (!compareIds(outputIds, network.outputIds)) {
            return false;
        }
        for (String outputId : outputIds) {
            Output output1 = (Output) nodeStorage.get(outputId);
            Output output2 = (Output) network.nodeStorage.get(outputId);
            if (output1 == null || output2 == null || !output1.equalsNeuron(output2)) {
                return false;
            }
        }
        if (neuronIds.length != network.neuronIds.length) {
            return false;
        }
        for (int i = 0; i < neuronIds.length; i++) {
            String[] layer1 = neuronIds[i];
            String[] layer2 = network.neuronIds[i];
            if (!compareIds(layer1, layer2)) {
                return false;
            }
            for (String neuronId : layer1) {
                Neuron neuron1 = (Neuron) nodeStorage.get(neuronId);
                Neuron neuron2 = (Neuron) network.nodeStorage.get(neuronId);
                if (neuron1 == null || neuron2 == null || !neuron1.equalsNeuron(neuron2)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean compareIds(String[] array1, String[] array2) {
        if (array1 == array2) {
            return true;
        }
        if (array1.length != array2.length) {
            return false;
        }
        HashSet<String> ids1 = new HashSet<>(array1.length);
        for (String id1 : array1) {
            if (ids1.contains(id1)) {
                return false;
            }
            ids1.add(id1);
        }
        HashSet<String> ids2 = new HashSet<>(array2.length);
        for (String id2 : array2) {
            if (ids2.contains(id2)) {
                return false;
            }
            ids2.add(id2);
        }
        for (String id2 : ids2) {
            if (!ids1.contains(id2)) {
                return false;
            }
        }
        return true;
    }

    public void setInputNumber(int inputNumber) {
        this.inputNumber = inputNumber;
    }

    public void setHiddenLayersNumber(int hiddenLayersNumber) {
        this.hiddenLayersNumber = hiddenLayersNumber;
    }

}
