/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mmv.nn.network.util.Utils;

public class Neuron extends Node {

    private static final long serialVersionUID = -8632981330544136404L;

    protected float errorValue;
    protected Map<String, Float> inputs = new HashMap<>();
    protected List<String> outputs = new ArrayList<>();
    protected Map<String, Node> nodeStorage;

    public Neuron(String id, Map<String, Node> nodeStorage) {
        super(id);
        this.nodeStorage = nodeStorage;
    }

    public Neuron(Map<String, Node> nodeStorage) {
        this.nodeStorage = nodeStorage;
    }

    public Map<String, Float> getInputs() {
        return inputs;
    }

    public List<String> getOutputs() {
        return outputs;
    }

    public float getErrorValue() {
        return errorValue;
    }

    public void setErrorValue(float errorValue) {
        this.errorValue = errorValue;
    }

    public void addInputConnection(String nodeId) {
        this.inputs.put(nodeId, Utils.getRandomNumber());
    }

    public void addOutputConnection(String neuronId) {
        this.outputs.add(neuronId);
    }

    @Override
    public void calculateOutputValue() {
        float accumulator = 0f;
        for (Map.Entry<String, Float> entry : inputs.entrySet()) {
            String inputId = entry.getKey();
            Node input = nodeStorage.get(inputId);
            Float weight = entry.getValue();
            accumulator += input.getOutputValue() * weight;
        }
        this.outputValue = Utils.sigmoid(accumulator);
    }

    public boolean equalsNeuron(Neuron neuron) {
        if (neuron == null) {
            return false;
        }
        if (this == neuron) {
            return true;
        }
        if (!id.equals(neuron.id)) {
            return false;
        }
        if (nodeStorage.size() != neuron.nodeStorage.size() || inputs.size() != neuron.inputs.size() || outputs.size() != neuron.outputs.size()) {
            return false;
        }
        for (Map.Entry<String, Float> entry : inputs.entrySet()) {
            String inputId = entry.getKey();
            Float weight = entry.getValue();
            if (!neuron.inputs.containsKey(inputId) || !weight.equals(neuron.inputs.get(inputId))) {
                return false;
            }
        }
        for (String outputId : outputs) {
            if (!neuron.outputs.contains(outputId)) {
                return false;
            }
        }
        return true;
    }
}
