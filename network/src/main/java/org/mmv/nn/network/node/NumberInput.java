/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.node;

public class NumberInput extends Input<Float> {

    private static final long serialVersionUID = -2369826890320109194L;

    public NumberInput() {
        super();
    }

    public NumberInput(String id) {
        super(id);
    }

    @Override
    public void calculateOutputValue() {
        this.outputValue = this.inputValue;
    }
}
