/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.util;

import java.time.Duration;
import java.util.HashMap;

public class Timer {

    private static final HashMap<String, Long> timers = new HashMap<>();

    public static void start(String timerName) {
        if (timerName == null) {
            return;
        }
        timers.put(timerName, System.currentTimeMillis());
    }

    public static void stop(String timerName) {
        if (timerName == null) {
            return;
        }
        Long startTime = timers.get(timerName);
        if (startTime == null) {
            return;
        }
        long currrentTime = System.currentTimeMillis();
        long timerTime = currrentTime - startTime;
        System.out.println("-> " + timerName + ": " + Duration.ofMillis(timerTime));
        timers.remove(timerName);
    }

    private Timer() {
    }
}
