/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.result;

public class Result<T> implements Comparable<Result<T>> {

    private final float floatValue;
    private final T value;
    private final Integer order;

    public Result(T value, float floatValue, Integer order) {
        this.value = value;
        this.floatValue = floatValue;
        this.order = order;
    }

    public String getBinaryProbability() {
        int probaility;
        if (floatValue >= 0.5) {
            probaility = getProbabilityFromRange(floatValue, 0.5f, 1, false);
        } else {
            probaility = getProbabilityFromRange(floatValue, 0, 0.5f, true);
        }
        return String.valueOf(probaility + "%");
    }

    public String getNumberProbability() {
        int result = (int) (floatValue * 100);
        return String.valueOf(result + "%");
    }

    private int getProbabilityFromRange(float x, float min, float max, boolean inverse) {
        float result = (x - min) * 100 / (max - min);
        if (inverse) {
            result = 100 - result;
        }
        return (int) result;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public T getValue() {
        return value;
    }

    public Integer getOrder() {
        return this.order;
    }

    @Override
    public int compareTo(Result<T> o) {
        return new Float(o.floatValue).compareTo(this.floatValue);
    }
}
