/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.node;

import java.io.Serializable;

import org.mmv.nn.network.util.Utils;

public abstract class Node implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String id;
    protected float outputValue;

    public Node() {
        this.id = Utils.generateId();
    }

    public Node(String id) {
        if (id == null || id.length() == 0) {
            throw new IllegalArgumentException("Node id cant't be null or empty!");
        }
        this.id = id;
    }

    public abstract void calculateOutputValue();

    public float getOutputValue() {
        return this.outputValue;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " id: " + this.id;
    }
}
