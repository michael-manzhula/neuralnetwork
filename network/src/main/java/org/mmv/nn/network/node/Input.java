/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.node;

public abstract class Input<T> extends Node {

    private static final long serialVersionUID = -2492985554448294197L;

    protected T inputValue;
    private Integer inputOrder;

    public Input() {
        super();
    }

    public Input(String id) {
        super(id);
    }

    public void setInputValue(T inputValue) {
        this.inputValue = inputValue;
    }

    public T getInputValue() {
        return this.inputValue;
    }

    public Integer getInputOrder() {
        return inputOrder;
    }

    public void setInputOrder(Integer order) {
        this.inputOrder = order;
    }

    @Override
    public abstract void calculateOutputValue();
}
