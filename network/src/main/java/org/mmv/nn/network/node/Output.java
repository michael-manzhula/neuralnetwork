/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.network.node;

import java.util.Map;

public abstract class Output<T> extends Neuron {

    private static final long serialVersionUID = -2961589716026710237L;
    private Integer outputOrder;

    public Output(String id, Map<String, Node> nodeStorage) {
        super(id, nodeStorage);
    }

    public Output(Map<String, Node> nodeStorage) {
        super(nodeStorage);
    }

    public Integer getOutputOrder() {
        return outputOrder;
    }

    public void setOutputOrder(Integer outputOrder) {
        this.outputOrder = outputOrder;
    }

    public abstract T getOutput();
}
