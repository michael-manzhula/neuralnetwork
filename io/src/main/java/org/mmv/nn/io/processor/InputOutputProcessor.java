/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.processor;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.node.NetworkEventHandler;

public interface InputOutputProcessor {

    public static final String SAVE_INPUTS = "Saving inputs";
    public static final String SAVE_NEURONS = "Saving neurons";
    public static final String SAVE_OUTPUTS = "Saving outputs";
    public static final String SAVE_LAYERS = "Saving layers";
    public static final String SAVE_NETWORK = "Saving network";

    public static final String LOAD_INPUTS = "Loading inputs";
    public static final String LOAD_NEURONS = "Loading neurons";
    public static final String LOAD_OUTPUTS = "Loading outputs";
    public static final String LOAD_LAYERS = "Loading layers";
    public static final String LOAD_NETWORK = "Loading network";

    public boolean exportNetwork(Network[] networks, String path, NetworkEventHandler<String> messageHandler) throws Exception;

    public Network[] importNetwork(String path, NetworkEventHandler<String> messageHandler) throws Exception;
}
