/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.mmv.nn.network.node.NumberInput;

@Entity
@Table(name = "NUMBER_INPUT")
public class NumberInputEntity implements Serializable {

    private static final long serialVersionUID = 7870026377370192540L;

    @Id
    private String id;
    private Integer networkId;
    private Float inputValue;
    private Integer inputOrder;
    @Version
    private Integer version;

    public NumberInputEntity() {
    }

    public NumberInputEntity(NumberInput input, int networkId) {
        this.id = input.getId();
        this.networkId = networkId;
        this.inputValue = input.getInputValue();
        this.inputOrder = input.getInputOrder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Integer networkId) {
        this.networkId = networkId;
    }

    public Float getInputValue() {
        return inputValue;
    }

    public void setInputValue(Float inputValue) {
        this.inputValue = inputValue;
    }

    public Integer getInputOrder() {
        return inputOrder;
    }

    public void setInputOrder(Integer inputOrder) {
        this.inputOrder = inputOrder;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
