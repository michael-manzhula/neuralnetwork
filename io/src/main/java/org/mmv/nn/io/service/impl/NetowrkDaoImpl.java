/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.service.impl;

import java.util.Set;

import javax.inject.Inject;

import org.mmv.nn.io.entity.NetworkEntity;
import org.mmv.nn.io.repository.NetworkRepository;
import org.mmv.nn.io.service.NetworkDao;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

@Service
@Repository
@Transactional
public class NetowrkDaoImpl implements NetworkDao {

    @Inject
    private NetworkRepository networkRepository;

    @Transactional(readOnly = true)
    @Override
    public NetworkEntity findById(Integer id) {
        return networkRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<NetworkEntity> findAll() {
        return Sets.newHashSet(networkRepository.findAll());
    }

    @Transactional
    @Override
    public NetworkEntity save(NetworkEntity entity) {
        return networkRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        networkRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        networkRepository.deleteAll();
    }

}
