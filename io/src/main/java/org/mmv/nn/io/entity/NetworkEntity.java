/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;

@Entity
@Table(name = "NETWORK")
public class NetworkEntity implements Serializable {

    private static final long serialVersionUID = 9085401658364025014L;

    @Id
    private Integer id;
    @Enumerated(EnumType.STRING)
    private NetworkType networkType;
    private Float learningRate;
    private Integer hiddenLayersNumber;
    @Version
    private Integer version;

    public NetworkEntity() {
    }

    public NetworkEntity(Network network) {
        this.id = network.getId();
        this.networkType = network.getNetworkType();
        this.learningRate = network.getLearningRate();
        this.hiddenLayersNumber = network.getHiddenLayersNumber();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NetworkType getNetworkType() {
        return networkType;
    }

    public void setNetworkType(NetworkType networkType) {
        this.networkType = networkType;
    }

    public Float getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(Float learningRate) {
        this.learningRate = learningRate;
    }

    public Integer getHiddenLayersNumber() {
        return hiddenLayersNumber;
    }

    public void setHiddenLayersNumber(Integer hiddenLayersNumber) {
        this.hiddenLayersNumber = hiddenLayersNumber;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
