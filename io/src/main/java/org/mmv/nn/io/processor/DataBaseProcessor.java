/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mmv.nn.io.configuration.DaoConfiguration;
import org.mmv.nn.io.converter.NetworkConverter;
import org.mmv.nn.io.entity.BinaryInputEntity;
import org.mmv.nn.io.entity.NetworkEntity;
import org.mmv.nn.io.entity.NeuronEntity;
import org.mmv.nn.io.entity.NeuronLayerEntity;
import org.mmv.nn.io.entity.NumberInputEntity;
import org.mmv.nn.io.entity.OutputEntity;
import org.mmv.nn.io.service.BinaryInputDao;
import org.mmv.nn.io.service.NetworkDao;
import org.mmv.nn.io.service.NeuronDao;
import org.mmv.nn.io.service.NeuronLayerDao;
import org.mmv.nn.io.service.NumberInputDao;
import org.mmv.nn.io.service.OutputDao;
import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.BinaryInput;
import org.mmv.nn.network.node.Input;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.node.Neuron;
import org.mmv.nn.network.node.Node;
import org.mmv.nn.network.node.NumberInput;
import org.mmv.nn.network.node.Output;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import javassist.tools.rmi.ObjectNotFoundException;

@SuppressWarnings("rawtypes")
public class DataBaseProcessor implements InputOutputProcessor {

    public DataBaseProcessor() {
    }

    @Override
    public boolean exportNetwork(Network[] networks, String path, NetworkEventHandler<String> messageHandler) throws Exception {
        if (networks == null || networks.length == 0 || path == null) {
            throw new IllegalArgumentException("Network or network path is null!");
        }
        sendMessage("Start saving network", messageHandler);
        DaoConfiguration.setPath(path);
        try (AnnotationConfigApplicationContext context = buildContext()) {
            int networkQuantity = networks.length;
            for (int i = 0; i < networks.length; i++) {
                Network network = networks[i];
                String prefix = String.format("Network %d/%d", i + 1, networkQuantity);
                saveInputs(network, context, messageHandler, prefix);
                saveNeurons(network, context, messageHandler, prefix);
                saveOutputs(network, context, messageHandler, prefix);
                saveLayers(network, context, messageHandler, prefix);
                saveNetwork(network, context, messageHandler, prefix);
            }
        }
        return true;
    }

    private void saveInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        if (NetworkType.BINARY == network.getNetworkType()) {
            saveBinaryInputs(network, context, messageHandler, prefix);
        } else if (NetworkType.NUMBER == network.getNetworkType()) {
            saveNumberInputs(network, context, messageHandler, prefix);
        }
    }

    private void saveBinaryInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        BinaryInputDao binaryInputDao = context.getBean(BinaryInputDao.class);
        binaryInputDao.deleteAll();
        String[] inputIds = network.getInputIds();
        int totalQuantity = inputIds.length;
        Map<String, Node> nodeStorage = network.getNodeStorage();
        for (int i = 0; i < totalQuantity; i++) {
            String inputId = inputIds[i];
            BinaryInput binaryInput = (BinaryInput) nodeStorage.get(inputId);
            BinaryInputEntity binaryInputEntity = new BinaryInputEntity(binaryInput, network.getId());
            sendMessage(String.format("%s %s: %d from %d", SAVE_INPUTS, prefix, i + 1, totalQuantity), messageHandler);
            binaryInputDao.save(binaryInputEntity);
        }
    }

    private void saveNumberInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NumberInputDao numberInputDao = context.getBean(NumberInputDao.class);
        numberInputDao.deleteAll();
        String[] inputIds = network.getInputIds();
        int totalQuantity = inputIds.length;
        Map<String, Node> nodeStorage = network.getNodeStorage();
        for (int i = 0; i < totalQuantity; i++) {
            String inputId = inputIds[i];
            NumberInput numberInput = (NumberInput) nodeStorage.get(inputId);
            NumberInputEntity numberInputEntity = new NumberInputEntity(numberInput, network.getId());
            sendMessage(String.format("%s %s: %d from %d", SAVE_INPUTS, prefix, i + 1, totalQuantity), messageHandler);
            numberInputDao.save(numberInputEntity);
        }
    }

    private void saveNeurons(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NeuronDao neuronDao = context.getBean(NeuronDao.class);
        neuronDao.deleteAll();
        String[][] neuronLayers = network.getNeuronIds();
        List<String> neuronIds = new ArrayList<>();
        for (String[] layerIds : neuronLayers) {
            neuronIds.addAll(Arrays.asList(layerIds));
        }
        int totalQantity = neuronIds.size();
        Map<String, Node> nodeStorage = network.getNodeStorage();
        for (int i = 0; i < totalQantity; i++) {
            String neuronId = neuronIds.get(i);
            Neuron neuron = (Neuron) nodeStorage.get(neuronId);
            NeuronEntity neuronEntity = new NeuronEntity(neuron, network.getId());
            sendMessage(String.format("%s %s: %d from %d", prefix, SAVE_NEURONS, i, totalQantity), messageHandler);
            neuronDao.save(neuronEntity);
        }
    }

    private void saveOutputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        OutputDao outputDao = context.getBean(OutputDao.class);
        outputDao.deleteAll();
        String[] outputIds = network.getOutputIds();
        int totalQuantity = outputIds.length;
        Map<String, Node> nodeStorage = network.getNodeStorage();
        for (int i = 0; i < totalQuantity; i++) {
            String outputId = outputIds[i];
            Output output = (Output) nodeStorage.get(outputId);
            OutputEntity outputEntity = new OutputEntity(output, network.getId());
            sendMessage(String.format("%s %s: %d from %d", SAVE_OUTPUTS, prefix, i + 1, totalQuantity), messageHandler);
            outputDao.save(outputEntity);
        }
    }

    private void saveLayers(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NeuronLayerDao neuronLayerDao = context.getBean(NeuronLayerDao.class);
        neuronLayerDao.deleteAll();
        String[][] neuronLayers = network.getNeuronIds();
        int totalQantity = neuronLayers.length;
        for (int i = 0; i < totalQantity; i++) {
            String[] neuronIds = neuronLayers[i];
            NeuronLayerEntity neuronLayerEntity = new NeuronLayerEntity();
            neuronLayerEntity.setNetworkId(network.getId());
            neuronLayerEntity.setLayerIndex(i);
            neuronLayerEntity.setNeuronIds(new HashSet<>(Arrays.asList(neuronIds)));
            sendMessage(String.format("%s %s: %d from %d", SAVE_LAYERS, prefix, i, totalQantity), messageHandler);
            neuronLayerDao.save(neuronLayerEntity);
        }
    }

    private void saveNetwork(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NetworkDao networkDao = context.getBean(NetworkDao.class);
        networkDao.deleteAll();
        NetworkEntity networkEntity = new NetworkEntity(network);
        sendMessage(String.format("%s %s...", prefix, SAVE_NETWORK), messageHandler);
        networkDao.save(networkEntity);
    }

    private void sendMessage(String message, NetworkEventHandler<String> messageHandler) {
        if (messageHandler == null) {
            return;
        }
        messageHandler.handle(message);
    }

    private AnnotationConfigApplicationContext buildContext() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("org.mmv.nn.io.configuration");
        context.refresh();
        return context;
    }

    @Override
    public Network[] importNetwork(String path, NetworkEventHandler<String> messageHandler) throws Exception {
        if (path == null) {
            throw new IllegalArgumentException("Network path is null!");
        }
        sendMessage("Start loading network", messageHandler);
        DaoConfiguration.setPath(path);
        try (AnnotationConfigApplicationContext context = buildContext()) {
            NetworkDao networkDao = context.getBean(NetworkDao.class);
            Set<NetworkEntity> networkEntities = networkDao.findAll();
            if (networkEntities.isEmpty()) {
                throw new ObjectNotFoundException("Network was not found!");
            }
            int networkQuantity = networkEntities.size();
            int currentNetworkIndex = 0;
            Network[] network = new Network[networkEntities.size()];
            for (NetworkEntity networkEntity : networkEntities) {
                String prefix = String.format("Network %d/%d", currentNetworkIndex + 1, networkQuantity);
                Network loadedNetwork = loadNetwork(networkEntity, context, messageHandler, prefix);
                if (loadedNetwork == null) {
                    throw new IllegalArgumentException("Network is null!");
                }
                network[currentNetworkIndex] = loadedNetwork;
                currentNetworkIndex++;
            }
            return network;
        }
    }

    private Network loadNetwork(NetworkEntity networkEntity, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        sendMessage(String.format("%s %s", prefix, LOAD_NETWORK), messageHandler);
        Network network = NetworkConverter.convertNetwork(networkEntity);

        loadLayers(network, context, messageHandler, prefix);
        loadInputs(network, context, messageHandler, prefix);
        loadNeurons(network, context, messageHandler, prefix);
        loadOutputs(network, context, messageHandler, prefix);

        return network;
    }

    private void loadLayers(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NeuronLayerDao neuronLayerDao = context.getBean(NeuronLayerDao.class);
        Set<NeuronLayerEntity> neuronLayerEntities = neuronLayerDao.findByNetworkId(network.getId());
        sendMessage(String.format("%s %s", prefix, LOAD_LAYERS), messageHandler);
        String[][] neuronLayers = new String[network.getHiddenLayersNumber()][];
        for (NeuronLayerEntity neuronLayerEntity : neuronLayerEntities) {
            int layerIndex = neuronLayerEntity.getLayerIndex();
            Set<String> neuronIds = neuronLayerEntity.getNeuronIds();
            neuronLayers[layerIndex] = neuronIds.toArray(new String[] {});
        }
        network.setNeuronIds(neuronLayers);
    }

    private void loadInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        if (NetworkType.BINARY == network.getNetworkType()) {
            loadBinaryInputs(network, context, messageHandler, prefix);
        } else if (NetworkType.NUMBER == network.getNetworkType()) {
            loadNumberInputs(network, context, messageHandler, prefix);
        }
    }

    private void loadNumberInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NumberInputDao inputDao = context.getBean(NumberInputDao.class);
        Set<NumberInputEntity> inputEntities = inputDao.findByNetworkId(network.getId());
        if (inputEntities.isEmpty()) {
            throw new IllegalArgumentException();
        }
        Map<String, Node> nodeStorage = network.getNodeStorage();
        List<Input> inputs = new ArrayList<>(inputEntities.size());
        int totalQuantity = inputEntities.size();
        int currentIndex = 1;
        for (NumberInputEntity inputEntity : inputEntities) {
            sendMessage(String.format("%s %s %d from %d", LOAD_INPUTS, prefix, currentIndex, totalQuantity), messageHandler);
            NumberInput input = NetworkConverter.convertNumberInput(inputEntity);
            String id = input.getId();
            nodeStorage.put(id, input);
            inputs.add(input);
        }
        addInputData(network, inputs);
    }

    private void loadBinaryInputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        BinaryInputDao inputDao = context.getBean(BinaryInputDao.class);
        Set<BinaryInputEntity> inputEntities = inputDao.findByNetworkId(network.getId());
        if (inputEntities.isEmpty()) {
            throw new IllegalArgumentException();
        }
        Map<String, Node> nodeStorage = network.getNodeStorage();
        List<Input> inputs = new ArrayList<>(inputEntities.size());
        int totalQuantity = inputEntities.size();
        int currentIndex = 1;
        for (BinaryInputEntity inputEntity : inputEntities) {
            sendMessage(String.format("%s %s %d from %d", LOAD_INPUTS, prefix, currentIndex, totalQuantity), messageHandler);
            BinaryInput input = NetworkConverter.convertBinaryInput(inputEntity);
            String id = input.getId();
            nodeStorage.put(id, input);
            inputs.add(input);
        }
        addInputData(network, inputs);
    }

    private void addInputData(Network network, List<Input> inputs) {
        inputs.sort(new Comparator<Input>() {

            @Override
            public int compare(Input o1, Input o2) {
                return o1.getInputOrder().compareTo(o2.getInputOrder());
            }
        });
        String[] inputIds = new String[inputs.size()];
        for (int i = 0; i < inputs.size(); i++) {
            inputIds[i] = inputs.get(i).getId();
        }
        network.setInputIds(inputIds);
        network.setInputNumber(inputIds.length);
    }

    private void loadNeurons(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        NeuronDao neuronDao = context.getBean(NeuronDao.class);
        sendMessage(String.format("%s %s", prefix, LOAD_NEURONS), messageHandler);
        List<String> neuronIds = new ArrayList<>();
        String[][] neuronLayers = network.getNeuronIds();
        for (String[] neuronLayer : neuronLayers) {
            neuronIds.addAll(Arrays.asList(neuronLayer));
        }
        int totalQuantity = neuronIds.size();
        int currentIndex = 1;
        Map<String, Node> nodeStorage = network.getNodeStorage();
        for (String neuronId : neuronIds) {
            sendMessage(String.format("%s %s %d of %d", prefix, LOAD_NEURONS, currentIndex, totalQuantity), messageHandler);
            NeuronEntity neuronEntity = neuronDao.findById(neuronId);
            Neuron neuron = NetworkConverter.convertNeuron(neuronEntity, nodeStorage);
            nodeStorage.put(neuron.getId(), neuron);
        }
    }

    private void loadOutputs(Network network, AbstractApplicationContext context, NetworkEventHandler<String> messageHandler, String prefix) {
        OutputDao outputDao = context.getBean(OutputDao.class);
        Set<OutputEntity> outputEntities = outputDao.findByNetworkId(network.getId());
        if (outputEntities.isEmpty()) {
            throw new IllegalArgumentException();
        }
        Map<String, Node> nodeStorage = network.getNodeStorage();
        List<Output> outputs = new ArrayList<>(outputEntities.size());
        int totalQuantity = outputEntities.size();
        int currentIndex = 1;
        for (OutputEntity outputEntity : outputEntities) {
            sendMessage(String.format("%s %s %d from %d", LOAD_OUTPUTS, prefix, currentIndex, totalQuantity), messageHandler);
            Output output = NetworkConverter.convertOutput(outputEntity, network);
            String id = output.getId();
            nodeStorage.put(id, output);
            outputs.add(output);
        }
        addOutputData(network, outputs);
    }

    private void addOutputData(Network network, List<Output> outputs) {
        outputs.sort(new Comparator<Output>() {

            @Override
            public int compare(Output o1, Output o2) {
                return o1.getOutputOrder().compareTo(o2.getOutputOrder());
            }
        });
        String[] outputIds = new String[outputs.size()];
        for (int i = 0; i < outputs.size(); i++) {
            outputIds[i] = outputs.get(i).getId();
        }
        network.setOutputIds(outputIds);
    }
}
