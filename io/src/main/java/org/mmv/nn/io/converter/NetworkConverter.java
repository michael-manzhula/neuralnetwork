/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.converter;

import java.util.Map;
import java.util.Set;

import org.mmv.nn.io.entity.BinaryInputEntity;
import org.mmv.nn.io.entity.NetworkEntity;
import org.mmv.nn.io.entity.NeuronEntity;
import org.mmv.nn.io.entity.NumberInputEntity;
import org.mmv.nn.io.entity.OutputEntity;
import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.BinaryInput;
import org.mmv.nn.network.node.BinaryOutput;
import org.mmv.nn.network.node.Neuron;
import org.mmv.nn.network.node.Node;
import org.mmv.nn.network.node.NumberInput;
import org.mmv.nn.network.node.NumberOutput;
import org.mmv.nn.network.node.Output;

@SuppressWarnings("rawtypes")
public class NetworkConverter {

    private NetworkConverter() {
    }

    public static Network convertNetwork(NetworkEntity networkEntity) {
        Network network = new Network(networkEntity.getId());
        network.setNetworkType(networkEntity.getNetworkType());
        network.setLearningRate(networkEntity.getLearningRate());
        network.setHiddenLayersNumber(networkEntity.getHiddenLayersNumber());
        return network;
    }

    public static NumberInput convertNumberInput(NumberInputEntity inputEntity) {
        NumberInput input = new NumberInput(inputEntity.getId());
        input.setInputValue(inputEntity.getInputValue());
        input.setInputOrder(inputEntity.getInputOrder());
        return input;
    }

    public static BinaryInput convertBinaryInput(BinaryInputEntity inputEntity) {
        BinaryInput input = new BinaryInput(inputEntity.getId());
        input.setInputValue(inputEntity.getInputValue());
        input.setInputOrder(inputEntity.getInputOrder());
        return input;
    }

    public static Neuron convertNeuron(NeuronEntity neuronEntity, Map<String, Node> nodeStorage) {
        Neuron neuron = new Neuron(neuronEntity.getId(), nodeStorage);
        Map<String, Float> inputs = neuronEntity.getInputs();
        Set<String> outputs = neuronEntity.getOutputs();
        addNeuronData(neuron, inputs, outputs);
        return neuron;
    }

    private static void addNeuronData(Neuron neuron, Map<String, Float> inputs, Set<String> outputs) {
        inputs.forEach((String nodeId, Float value) -> {
            neuron.getInputs().put(nodeId, value);
        });
        outputs.forEach((String outputId) -> neuron.getOutputs().add(outputId));
    }

    public static Output convertOutput(OutputEntity outputEntity, Network network) {
        Output output = null;
        if (NetworkType.BINARY == network.getNetworkType()) {
            output = new BinaryOutput(outputEntity.getId(), network.getNodeStorage());
        } else if (NetworkType.NUMBER == network.getNetworkType()) {
            output = new NumberOutput(outputEntity.getId(), network.getNodeStorage());
        }
        if (output == null) {
            throw new IllegalArgumentException();
        }
        Map<String, Float> inputs = outputEntity.getInputs();
        Set<String> outputs = outputEntity.getOutputs();
        addNeuronData(output, inputs, outputs);
        output.setOutputOrder(outputEntity.getOutputOrder());
        return output;
    }
}
