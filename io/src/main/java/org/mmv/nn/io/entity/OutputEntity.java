/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.mmv.nn.network.node.Output;

@Entity
@Table(name = "OUTPUT_NODE")
public class OutputEntity implements Serializable {

    private static final long serialVersionUID = -899196136533155270L;

    @Id
    private String id;
    private Integer networkId;
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Float.class)
    @CollectionTable(name = "output_input_ids")
    private Map<String, Float> inputs = new HashMap<>();
    @ElementCollection(fetch = FetchType.EAGER, targetClass = String.class)
    @CollectionTable(name = "output_output_ids")
    private Set<String> outputs = new HashSet<>();
    private Integer outputOrder;
    @Version
    private Integer version;

    public OutputEntity() {
    }

    @SuppressWarnings("rawtypes")
    public OutputEntity(Output output, int networkId) {
        this.id = output.getId();
        this.networkId = networkId;
        for (Map.Entry<String, Float> entry : output.getInputs().entrySet()) {
            this.inputs.put(entry.getKey(), entry.getValue());
        }
        for (String outputId : output.getOutputs()) {
            this.outputs.add(outputId);
        }
        this.outputOrder = output.getOutputOrder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Integer networkId) {
        this.networkId = networkId;
    }

    public Map<String, Float> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, Float> inputs) {
        this.inputs = inputs;
    }

    public Set<String> getOutputs() {
        return outputs;
    }

    public void setOutputs(Set<String> outputs) {
        this.outputs = outputs;
    }

    public Integer getOutputOrder() {
        return outputOrder;
    }

    public void setOutputOrder(Integer outputOrder) {
        this.outputOrder = outputOrder;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
