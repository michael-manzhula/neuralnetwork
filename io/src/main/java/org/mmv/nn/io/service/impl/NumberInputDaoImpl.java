/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.service.impl;

import java.util.Set;

import javax.inject.Inject;

import org.mmv.nn.io.entity.NumberInputEntity;
import org.mmv.nn.io.repository.NumberInputRepository;
import org.mmv.nn.io.service.NumberInputDao;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

@Service
@Repository
@Transactional
public class NumberInputDaoImpl implements NumberInputDao {

    @Inject
    private NumberInputRepository numberInputRepository;

    @Transactional(readOnly = true)
    @Override
    public NumberInputEntity findById(String id) {
        return numberInputRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<NumberInputEntity> findByNetworkId(Integer networkId) {
        return numberInputRepository.findByNetworkId(networkId);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<NumberInputEntity> findAll() {
        return Sets.newHashSet(numberInputRepository.findAll());
    }

    @Transactional
    @Override
    public NumberInputEntity save(NumberInputEntity entity) {
        return numberInputRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(String id) {
        numberInputRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        numberInputRepository.deleteAll();
    }

}
