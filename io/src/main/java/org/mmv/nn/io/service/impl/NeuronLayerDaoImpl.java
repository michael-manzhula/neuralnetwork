/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.service.impl;

import java.util.Set;

import javax.inject.Inject;

import org.mmv.nn.io.entity.NeuronLayerEntity;
import org.mmv.nn.io.repository.NeuronLayerRepository;
import org.mmv.nn.io.service.NeuronLayerDao;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

@Service
@Repository
@Transactional
public class NeuronLayerDaoImpl implements NeuronLayerDao {

    @Inject
    private NeuronLayerRepository neuronLayerRepository;

    @Transactional(readOnly = true)
    @Override
    public NeuronLayerEntity findById(Integer id) {
        return neuronLayerRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<NeuronLayerEntity> findAll() {
        return Sets.newHashSet(neuronLayerRepository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Set<NeuronLayerEntity> findByNetworkId(Integer networkId) {
        return neuronLayerRepository.findByNetworkId(networkId);
    }

    @Transactional
    @Override
    public NeuronLayerEntity save(NeuronLayerEntity entity) {
        return neuronLayerRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        neuronLayerRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        neuronLayerRepository.deleteAll();
    }

}
