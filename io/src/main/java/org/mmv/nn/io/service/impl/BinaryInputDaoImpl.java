/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.io.service.impl;

import java.util.Set;

import javax.inject.Inject;

import org.mmv.nn.io.entity.BinaryInputEntity;
import org.mmv.nn.io.repository.BinaryInputRepository;
import org.mmv.nn.io.service.BinaryInputDao;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

@Service
@Repository
@Transactional
public class BinaryInputDaoImpl implements BinaryInputDao {

    @Inject
    private BinaryInputRepository binaryInputRepository;

    @Transactional(readOnly = true)
    @Override
    public BinaryInputEntity findById(String id) {
        return binaryInputRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<BinaryInputEntity> findAll() {
        return Sets.newHashSet(binaryInputRepository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Set<BinaryInputEntity> findByNetworkId(Integer networkId) {
        return binaryInputRepository.findByNetworkId(networkId);
    }

    @Transactional
    @Override
    public BinaryInputEntity save(BinaryInputEntity entity) {
        return binaryInputRepository.save(entity);
    }

    @Transactional
    @Override
    public void delete(String id) {
        binaryInputRepository.delete(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        binaryInputRepository.deleteAll();
    }

}
