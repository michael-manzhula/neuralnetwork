/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.controller;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.result.Result;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class BinaryNetworkController extends BaseController implements Initializable, TabController {

    @FXML
    private Button binaryCreateButton;
    @FXML
    private Button binaryEmitButton;
    @FXML
    private Button binaryTeachButton;
    @FXML
    private Button binaryDisposeButton;
    @FXML
    private Button binaryClearOutputButton;
    @FXML
    protected TextField binaryLayersTextField;
    @FXML
    private ToggleButton b1;
    @FXML
    private ToggleButton b2;
    @FXML
    private ToggleButton b3;
    @FXML
    private TextField binaryLearningCyclesTextField;
    @FXML
    private TextArea binaryOutputArea;
    @FXML
    private Label binaryStatusLabel;
    @FXML
    protected TextField binaryLearningRateTextField;
    @FXML
    private ToggleButton binary000CombinationButton;
    @FXML
    private ToggleButton binary001CombinationButton;
    @FXML
    private ToggleButton binary010CombinationButton;
    @FXML
    private ToggleButton binary011CombinationButton;
    @FXML
    private ToggleButton binary100CombinationButton;
    @FXML
    private ToggleButton binary101CombinationButton;
    @FXML
    private ToggleButton binary110CombinationButton;
    @FXML
    private ToggleButton binary111CombinationButton;
    private Network binaryNetwork;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        binaryCreateButton.disableProperty().bind(interfaceStatus);
        binaryEmitButton.disableProperty().bind(interfaceStatus);
        binaryTeachButton.disableProperty().bind(interfaceStatus);
        binaryDisposeButton.disableProperty().bind(interfaceStatus);
        binaryClearOutputButton.disableProperty().bind(interfaceStatus);
        binaryStatusLabel.setText("");
        binaryLearningRateTextField.disableProperty().bind(interfaceStatus);
        binaryLearningCyclesTextField.disableProperty().bind(interfaceStatus);
        binaryLayersTextField.disableProperty().bind(interfaceStatus);
        b1.setOnAction((event) -> b1.setText(getStringFromToggleButton(b1)));
        b1.disableProperty().bind(interfaceStatus);
        b2.setOnAction((event) -> b2.setText(getStringFromToggleButton(b2)));
        b2.disableProperty().bind(interfaceStatus);
        b3.setOnAction((event) -> b3.setText(getStringFromToggleButton(b3)));
        b3.disableProperty().bind(interfaceStatus);

        binary000CombinationButton.setOnAction((event) -> binary000CombinationButton.setText(getStringFromToggleButton(binary000CombinationButton)));
        binary000CombinationButton.disableProperty().bind(interfaceStatus);
        binary001CombinationButton.setOnAction((event) -> binary001CombinationButton.setText(getStringFromToggleButton(binary001CombinationButton)));
        binary001CombinationButton.disableProperty().bind(interfaceStatus);
        binary010CombinationButton.setOnAction((event) -> binary010CombinationButton.setText(getStringFromToggleButton(binary010CombinationButton)));
        binary010CombinationButton.disableProperty().bind(interfaceStatus);
        binary011CombinationButton.setOnAction((event) -> binary011CombinationButton.setText(getStringFromToggleButton(binary011CombinationButton)));
        binary011CombinationButton.disableProperty().bind(interfaceStatus);
        binary100CombinationButton.setOnAction((event) -> binary100CombinationButton.setText(getStringFromToggleButton(binary100CombinationButton)));
        binary100CombinationButton.disableProperty().bind(interfaceStatus);
        binary101CombinationButton.setOnAction((event) -> binary101CombinationButton.setText(getStringFromToggleButton(binary101CombinationButton)));
        binary101CombinationButton.disableProperty().bind(interfaceStatus);
        binary110CombinationButton.setOnAction((event) -> binary110CombinationButton.setText(getStringFromToggleButton(binary110CombinationButton)));
        binary110CombinationButton.disableProperty().bind(interfaceStatus);
        binary111CombinationButton.setOnAction((event) -> binary111CombinationButton.setText(getStringFromToggleButton(binary111CombinationButton)));
        binary111CombinationButton.disableProperty().bind(interfaceStatus);
    }

    @FXML
    private void binaryCreate() {
        if (binaryNetwork != null) {
            ButtonType answer = showDialog("Network is already created! Do you want to create a new network?", AlertType.CONFIRMATION);
            if (ButtonType.OK != answer) {
                return;
            }
        }
        float learningRate;
        int neuronLayersQuantity;
        try {
            learningRate = Float.parseFloat(binaryLearningRateTextField.getText());
            neuronLayersQuantity = Integer.parseInt(binaryLayersTextField.getText());
        } catch (NumberFormatException ex) {
            String message = "Configuration is not correct! " + ex.getMessage();
            showDialog(message, AlertType.ERROR);
            return;
        }
        toggleUserInterface(false);
        NetworkEventHandler<Network> handler = (Network network) -> {
            if (network == null) {
                showDialog("Error occured while creating network!", AlertType.ERROR);
            } else {
                network.setId(1);
                binaryNetwork = network;
                binaryStatusLabel.setText("Created");
                showDialog("Binary network is created!", AlertType.INFORMATION);
            }
            toggleUserInterface(true);
        };
        StringProperty statusTextProperty = binaryStatusLabel.textProperty();
        networkProcessor.createNetwork(3, 1, neuronLayersQuantity, learningRate, NetworkType.BINARY, statusTextProperty, handler);
    }

    @FXML
    private void binaryClearOutput() {
        this.binaryOutputArea.clear();
    }

    @FXML
    private void binaryTeachNetwork() {
        if (binaryNetwork == null) {
            showDialog("Network is not created!", AlertType.WARNING);
            return;
        }
        String cycleQuantityString = binaryLearningCyclesTextField.getText();
        int cycleQuantity;
        try {
            cycleQuantity = Integer.parseInt(cycleQuantityString);
        } catch (NumberFormatException e) {
            showDialog("The quantity of cycles is not correct! " + e.getMessage(), AlertType.ERROR);
            return;
        }
        Map<Boolean[], Boolean> learningData = new HashMap<>();
        learningData.put(createBinaryInputData("000"), binary000CombinationButton.isSelected());
        learningData.put(createBinaryInputData("001"), binary001CombinationButton.isSelected());
        learningData.put(createBinaryInputData("010"), binary010CombinationButton.isSelected());
        learningData.put(createBinaryInputData("011"), binary011CombinationButton.isSelected());
        learningData.put(createBinaryInputData("100"), binary100CombinationButton.isSelected());
        learningData.put(createBinaryInputData("101"), binary101CombinationButton.isSelected());
        learningData.put(createBinaryInputData("110"), binary110CombinationButton.isSelected());
        learningData.put(createBinaryInputData("111"), binary111CombinationButton.isSelected());
        Alert cancelAlert = createCancelAlert();
        StringProperty statusTextProperty = cancelAlert.contentTextProperty();
        NetworkEventHandler<Boolean> handler = (Boolean result) -> {
            binaryStatusLabel.setText("Ok");
            toggleUserInterface(true);
            if (cancelAlert.isShowing()) {
                cancelAlert.close();
            }
            if (result) {
                showDialog("Learning finished!", AlertType.INFORMATION);
            } else {
                showDialog("Error occured during learning!", AlertType.ERROR);
            }
            toggleUserInterface(true);
        };
        toggleUserInterface(false);
        cancelRequest.set(false);
        networkProcessor.teachBinaryNetwork(binaryNetwork, cycleQuantity, learningData, statusTextProperty, handler, cancelRequest);
        cancelAlert.setOnCloseRequest((event) -> cancelRequest.set(true));
        cancelAlert.show();
    }

    private Boolean[] createBinaryInputData(String combination) {
        Boolean[] inputData = new Boolean[combination.length()];
        int i = 0;
        for (int x = 0; x < inputData.length; x++) {
            Boolean value = "1".equals(combination.substring(i, i + 1));
            inputData[x] = value;
            i++;
        }
        return inputData;
    }

    @FXML
    private void binaryEmit() {
        if (binaryNetwork == null) {
            showDialog("Network is not created!", AlertType.WARNING);
            return;
        }
        Boolean[] inputData = new Boolean[3];
        inputData[0] = b1.isSelected();
        inputData[1] = b2.isSelected();
        inputData[2] = b3.isSelected();

        StringBuilder outputBuffer = new StringBuilder("Emit:\n");
        for (int x = 0; x < inputData.length; x++) {
            boolean value = inputData[x];
            int intValue = value ? 1 : 0;
            outputBuffer.append(String.valueOf(intValue) + "  ");
        }
        outputBuffer.append("\n");
        NetworkEventHandler<Result<Boolean>> handler = new NetworkEventHandler<Result<Boolean>>() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void handle(Result<Boolean> result) {
                if (result == null) {
                    showDialog("Error!", AlertType.ERROR);
                } else {
                    outputBuffer.append("Correct result is: " + result.getValue().toString().toUpperCase() + " with probability " + result.getBinaryProbability() + "\n\n");
                    binaryOutputArea.insertText(0, outputBuffer.toString());
                }
                toggleUserInterface(true);
            }
        };
        toggleUserInterface(false);
        binaryNetwork.analyze(inputData, handler);
    }

    @FXML
    private void binaryDispose() {
        ButtonType answer = showDialog("Are you sure?", AlertType.CONFIRMATION);
        if (ButtonType.OK == answer) {
            binaryNetwork = null;
            System.gc();
            binaryStatusLabel.setText("Disposed");
        }
    }

    private String getStringFromToggleButton(ToggleButton button) {
        if (button.isSelected()) {
            return "1";
        }
        return "0";
    }

    @Override
    public void setNetwork(Network[] network) {
        binaryNetwork = network[0];
        binaryStatusLabel.setText("Loaded");
    }

    @Override
    public void saveNetwork() {
        super.saveNetwork(new Network[] { binaryNetwork });
    }
}
