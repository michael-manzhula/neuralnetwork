/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.controller;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.ui.processor.ImportExportProcessor;
import org.mmv.nn.ui.processor.NetworkProcessor;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public abstract class BaseController {

    private static final String DIALOG_HEADER = "Attention!";
    private static final String SAVE_NETWORK_TITLE = "Save neural network";

    protected File previousLocation;
    protected AtomicBoolean cancelRequest = new AtomicBoolean(false);
    protected ImportExportProcessor importExportProcessor;
    protected NetworkProcessor networkProcessor;
    protected Stage stage;
    protected BooleanProperty interfaceStatus;

    public BaseController() {
        interfaceStatus = new SimpleBooleanProperty(false);
        importExportProcessor = new ImportExportProcessor();
        networkProcessor = new NetworkProcessor();
    }

    protected void toggleUserInterface(boolean isEnabled) {
        interfaceStatus.setValue(!isEnabled);
    }

    protected void saveNetwork(Network[] networks) {
        ButtonType answer = showDialog("It can take long time... Do you want to continue?", AlertType.CONFIRMATION);
        if (ButtonType.OK != answer) {
            return;
        }
        if (networks == null || networks.length == 0) {
            showDialog("Network is not created!", AlertType.INFORMATION);
            return;
        }
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(SAVE_NETWORK_TITLE);
        if (previousLocation != null) {
            directoryChooser.setInitialDirectory(previousLocation);
        }
        File choosenDirectory = directoryChooser.showDialog(stage);
        if (choosenDirectory == null) {
            return;
        }
        previousLocation = choosenDirectory;
        String path = getNetworkPath(choosenDirectory, networks[0].getNetworkType());
        Alert messageDialog = showMessageDialog();
        NetworkEventHandler<Boolean> handler = new NetworkEventHandler<Boolean>() {
            @Override
            public void handle(Boolean result) {
                closeAlert(messageDialog);
                if (result) {
                    ButtonType validationAnswer = showDialog("Network is saved. Do you want to validate it?", AlertType.CONFIRMATION);
                    if (ButtonType.OK == validationAnswer) {
                        validateSavedNetwork(path, networks);
                    } else {
                        toggleUserInterface(true);
                    }
                } else {
                    showDialog("Error occured!", AlertType.ERROR);
                    toggleUserInterface(true);
                }
            }
        };
        toggleUserInterface(false);
        importExportProcessor.exportNetwork(networks, handler, path, messageDialog.contentTextProperty());
    }

    public void validateSavedNetwork(String path, Network[] originNetwork) {
        Alert messageDialog = showMessageDialog();
        NetworkEventHandler<Network[]> loadHandler = new NetworkEventHandler<Network[]>() {
            @Override
            public void handle(Network[] loadedNetwork) {
                messageDialog.setContentText("Validating network...");
                if (loadedNetwork != null && isNetworkEqual(originNetwork, loadedNetwork)) {
                    closeAlert(messageDialog);
                    showDialog("Network was successfully validated!", AlertType.INFORMATION);
                } else {
                    closeAlert(messageDialog);
                    showDialog("Saved network is corrupt!", AlertType.WARNING);
                }
                toggleUserInterface(true);
            }
        };
        toggleUserInterface(false);
        importExportProcessor.importNetwork(loadHandler, path, messageDialog.contentTextProperty());
    }

    public boolean isNetworkEqual(Network[] originNetwork, Network[] loadedNetwork) {
        if (originNetwork == null || originNetwork.length == 0) {
            return false;
        }
        if (loadedNetwork == null || loadedNetwork.length == 0) {
            return false;
        }
        if (originNetwork.length != loadedNetwork.length) {
            return false;
        }
        Comparator<Network> networkComparator = new Comparator<Network>() {

            @Override
            public int compare(Network o1, Network o2) {
                return o1.getId() - o2.getId();
            }
        };
        Arrays.sort(originNetwork, networkComparator);
        Arrays.sort(loadedNetwork, networkComparator);
        for (int i = 0; i < originNetwork.length; i++) {
            Network origin = originNetwork[i];
            Network loaded = loadedNetwork[i];
            if (!origin.equalsNetwork(loaded)) {
                return false;
            }
        }
        return true;
    }

    protected ButtonType showDialog(String message, AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(DIALOG_HEADER);
        alert.setHeaderText(null);
        alert.setContentText(message);
        return alert.showAndWait().get();
    }

    protected Alert showMessageDialog() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.getButtonTypes().clear();
        alert.setTitle(SAVE_NETWORK_TITLE);
        alert.setHeaderText(null);
        alert.show();
        return alert;
    }

    protected void closeAlert(Alert alert) {
        alert.getButtonTypes().add(ButtonType.CANCEL);
        ButtonType buttonType = alert.getButtonTypes().get(0);
        if (buttonType != null) {
            Button button = (Button) alert.getDialogPane().lookupButton(buttonType);
            button.fire();
        }
    }

    protected Alert createCancelAlert() {
        Alert alert = new Alert(AlertType.NONE);
        alert.setTitle(DIALOG_HEADER);
        alert.setHeaderText("Press 'Cancel' to stop process.");
        ButtonType button = new ButtonType("Cancel");
        alert.getButtonTypes().add(button);
        return alert;
    }

    private String getNetworkPath(File choosenDirectory, NetworkType networkType) {
        StringBuilder networkPath = new StringBuilder();
        networkPath.append(choosenDirectory.getAbsolutePath());
        if (NetworkType.BINARY == networkType) {
            networkPath.append("/binary_network_");
        } else if (NetworkType.NUMBER == networkType) {
            networkPath.append("/number_network_");
        } else {
            return null;
        }
        String date = String.valueOf(new Date().getTime());
        networkPath.append(date);
        return networkPath.toString();
    }

    protected String showPromptDialog(String message) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setHeaderText(null);
        dialog.setGraphic(null);
        dialog.setTitle("Attention!");
        dialog.setContentText(message);
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        }
        return null;
    }
}
