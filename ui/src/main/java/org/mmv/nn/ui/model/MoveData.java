package org.mmv.nn.ui.model;

public class MoveData {
    public int moveOrder;
    public int cellNumber;
    public int[] gameField;

    public MoveData(int moveOrder, int cellIndex, int[] gameField) {
        this.moveOrder = moveOrder;
        this.cellNumber = cellIndex;
        this.gameField = new int[gameField.length];
        for (int i = 0; i < gameField.length; i++) {
            this.gameField[i] = gameField[i];
        }
    }
}
