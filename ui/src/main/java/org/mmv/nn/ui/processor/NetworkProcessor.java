/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.result.Result;
import org.mmv.nn.ui.MainApp;
import org.mmv.nn.ui.controller.XONetworkController;
import org.mmv.nn.ui.model.GameData;
import org.mmv.nn.ui.model.MoveData;
import org.mmv.nn.ui.model.StatusData;

import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class NetworkProcessor {

    private static final String CYCLES_LEFT = "Cycles left: ";

    private final Logger logger = Logger.getLogger(NetworkProcessor.class.getName());

    public void teachBinaryNetwork(Network network, int cycleQuantity, Map<Boolean[], Boolean> learningData, StringProperty statusProperty, NetworkEventHandler<Boolean> handler, AtomicBoolean cancelRequest) {
        Task<Void> learningTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < cycleQuantity; i++) {
                    for (Map.Entry<Boolean[], Boolean> entry : learningData.entrySet()) {
                        Boolean[] inputData = entry.getKey();
                        network.analyze(inputData, (result) -> {
                            // Result is ignored
                        });
                        network.learn(entry.getValue());
                        updateMessage(CYCLES_LEFT + String.valueOf(cycleQuantity - i));
                        if (cancelRequest.get()) {
                            return null;
                        }
                    }
                }
                return null;
            }
        };
        teachNetwork(learningTask, statusProperty, handler);
    }

    public void teachNumberNetwork(Network network, int cycleQuantity, Map<Float[], Integer> learningData, StringProperty statusProperty, NetworkEventHandler<Boolean> handler, AtomicBoolean cancelRequest) {
        Task<Void> learningTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < cycleQuantity; i++) {
                    for (Map.Entry<Float[], Integer> entry : learningData.entrySet()) {
                        Float[] inputData = entry.getKey();
                        network.analyze(inputData, (result) -> {
                            // Result is ignored
                        });
                        network.learn(entry.getValue());
                        updateMessage(CYCLES_LEFT + String.valueOf(cycleQuantity - i));
                        if (cancelRequest.get()) {
                            return null;
                        }
                    }
                }
                return null;
            }
        };
        teachNetwork(learningTask, statusProperty, handler);
    }

    private <T> void teachNetwork(Task<T> learningTask, StringProperty statusProperty, NetworkEventHandler<Boolean> handler) {
        statusProperty.bind(learningTask.messageProperty());
        learningTask.setOnSucceeded((event) -> {
            statusProperty.unbind();
            handler.handle(true);
        });
        learningTask.setOnFailed((event) -> {
            statusProperty.unbind();
            handler.handle(false);
        });
        new Thread(learningTask).start();
    }

    public void createNetwork(int inputNumber, int outputNumber, int layersQuantity, float learningRate, NetworkType networkType, StringProperty statusProperty, NetworkEventHandler<Network> handler) {
        Task<Network> task = new Task<Network>() {
            @Override
            protected Network call() throws Exception {
                return new Network(inputNumber, outputNumber, layersQuantity, learningRate, networkType, (message) -> updateMessage(message));
            }
        };
        statusProperty.bind(task.messageProperty());
        task.setOnSucceeded((event) -> {
            statusProperty.unbind();
            handler.handle(task.getValue());
        });
        task.setOnFailed((event) -> {
            statusProperty.unbind();
            String message = event.getSource().getException().getMessage();
            logger.log(Level.SEVERE, message);
            handler.handle(null);
        });
        new Thread(task).start();
    }

    public int[] convertImageToArray() {
        return null;
    }

    public static Float[] getNumberInputData(WritableImage image) {
        int width = (int) image.getWidth();
        int height = (int) image.getHeight();
        PixelReader pixelReader = image.getPixelReader();
        if (pixelReader == null) {
            return null;
        }
        List<Float> pixelData = new ArrayList<>(height * width);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Color c = pixelReader.getColor(x, y);
                double red = c.getRed();
                double green = c.getGreen();
                double blue = c.getBlue();
                double average = (red + green + blue) / 3;
                pixelData.add((float) average);
            }
        }
        return pixelData.toArray(new Float[pixelData.size()]);
    }

    public void startInternalXOGame(final GameData gameData, final NetworkEventHandler<StatusData> handler, AtomicBoolean cancelRequest, StringProperty statusProperty) {
        Task<Void> task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < gameData.cycleQuantity; i++) {
                    if (MainApp.DEBUG_MODE) {
                        System.out.println("New game: " + (i + 1));
                    }
                    processGameCycle(gameData);
                    gameData.cycleQuantity--;
                    updateMessage(CYCLES_LEFT + gameData.cycleQuantity);
                    if (cancelRequest.get()) {
                        return null;
                    }
                }
                return null;
            }
        };
        statusProperty.bind(task.messageProperty());
        task.setOnSucceeded((event) -> {
            statusProperty.unbind();
            handler.handle(new StatusData(true, false));
        });
        task.setOnFailed((event) -> {
            statusProperty.unbind();
            handler.handle(new StatusData(false, true));
        });
        new Thread(task).start();
    }

    public void processGameCycle(GameData gameData) {
        int cellType = XONetworkController.X_CELL;
        boolean learningRequired = true;
        Boolean gameStatus;
        do {
            final List<MoveData> playerMoves = gameData.getMoves(cellType);
            final Network network = gameData.getNetwork(cellType, playerMoves.size());
            final int[] gameField = gameData.gameField;
            Float[] inputData = new Float[gameField.length];
            for (int i = 0; i < gameField.length; i++) {
                inputData[i] = (float) gameField[i];
            }
            Result<Float>[] results = network.analyzeSync(inputData);
            if (results == null) {
                // TODO: handle error
            } else {
                int cellIndex = getCellIndex(gameField, results);
                gameField[cellIndex] = cellType;
                MoveData moveData = new MoveData(playerMoves.size(), cellIndex, gameField);
                playerMoves.add(moveData);
                cellType = switchCellType(cellType);
            }
            gameStatus = checkGameStatus(gameData.gameField);
            if (gameStatus == null) {
                learningRequired = false;
            }
        } while (gameStatus != null && !gameStatus);
        if (learningRequired) {
            learnLoop(gameData, switchCellType(cellType), true, 0);
        }
        cleanGameData(gameData);
    }

    private int switchCellType(int cellType) {
        return XONetworkController.X_CELL == cellType ? XONetworkController.O_CELL : XONetworkController.X_CELL;
    }

    public Boolean checkGameStatus(int[] gameField) {
        if (MainApp.DEBUG_MODE) {
            System.out.println("-> Game field:");
            int r = 0;
            StringBuilder row = new StringBuilder();
            for (int i = 0; i < gameField.length; i++) {
                int cellValue = gameField[i];
                if (XONetworkController.X_CELL == cellValue) {
                    row.append(" X");
                } else if (XONetworkController.O_CELL == cellValue) {
                    row.append(" O");
                } else {
                    row.append(" -");
                }
                r++;
                if (r == 3) {
                    r = 0;
                    System.out.println(row);
                    row.setLength(0);
                }
            }
        }
        boolean isFreeCellsLeft = false;
        for (int cellType : gameField) {
            if (XONetworkController.FREE_CELL == cellType) {
                isFreeCellsLeft = true;
                break;
            }
        }
        for (int[] line : XONetworkController.LINES) {
            if (gameField[line[0]] != XONetworkController.FREE_CELL && gameField[line[0]] == gameField[line[1]] && gameField[line[1]] == gameField[line[2]]) {
                return true;
            }
        }
        if (!isFreeCellsLeft) {
            return null;
        }
        return false;
    }

    public int getCellIndex(int[] gameField, Result<Float>[] results) {
        // Find best result for free cells at the game field
        List<Result<Float>> resultsForFreeCells = new ArrayList<>();
        for (int i = 0; i < gameField.length; i++) {
            if (gameField[i] == XONetworkController.FREE_CELL) {
                resultsForFreeCells.add(results[i]);
            }
        }
        Collections.sort(resultsForFreeCells);
        Result<Float> bestResult = resultsForFreeCells.get(0);
        return bestResult.getOrder();
    }

    private void learnLoop(GameData gameData, int cellType, boolean isWinner, int order) {
        List<MoveData> playerMoves = gameData.getMoves(cellType);
        if (playerMoves.size() > order) {
            Network network = gameData.getNetworkGroup(cellType)[order];
            MoveData moveData = gameData.getMoveData(cellType, order);
            network.learn(moveData.cellNumber, isWinner);
            learnLoop(gameData, cellType, isWinner, order + 1);
        } else {
            if (isWinner) {
                int nextCellType = switchCellType(cellType);
                learnLoop(gameData, nextCellType, false, 0);
            }
        }
    }

    private void cleanGameData(GameData gameData) {
        gameData.gameField = new int[XONetworkController.GAME_FIELD_SIZE];
        gameData.player1Moves.clear();
        gameData.player2Moves.clear();
    }
}
