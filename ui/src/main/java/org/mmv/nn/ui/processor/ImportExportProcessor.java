/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.processor;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.mmv.nn.io.processor.InputOutputProcessor;
import org.mmv.nn.io.processor.InputOutputProcessorFactory;
import org.mmv.nn.network.Network;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.ui.util.Utils;

import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class ImportExportProcessor {

    private final Logger logger = Logger.getLogger(ImportExportProcessor.class.getName());

    private static final String IMAGE_DATA_FOLDER = "/image_data/";
    private static final String IMAGE_FILE_EXTENSION = "png";

    public void exportNetwork(Network[] networks, NetworkEventHandler<Boolean> handler, String networkDirectory, StringProperty messageProperty) {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                InputOutputProcessor processor = InputOutputProcessorFactory.buildProcessor();
                processor.exportNetwork(networks, networkDirectory, (String message) -> updateMessage(message));
                return true;
            }
        };
        messageProperty.bind(task.messageProperty());
        task.setOnSucceeded((event) -> {
            messageProperty.unbind();
            handler.handle(true);
        });
        task.setOnFailed((event) -> {
            messageProperty.unbind();
            String message = event.getSource().getException().getMessage();
            logger.log(Level.SEVERE, message);
            handler.handle(false);
        });
        new Thread(task).start();
    }

    public void importNetwork(NetworkEventHandler<Network[]> handler, String networkPath, StringProperty messageProperty) {
        if (networkPath == null) {
            handler.handle(null);
        }
        Task<Network[]> task = new Task<Network[]>() {
            @Override
            protected Network[] call() throws Exception {
                InputOutputProcessor processor = InputOutputProcessorFactory.buildProcessor();
                Network[] network = processor.importNetwork(networkPath, (String message) -> updateMessage(message));
                return network;
            }
        };
        messageProperty.bind(task.messageProperty());
        task.setOnSucceeded((event) -> {
            messageProperty.unbind();
            handler.handle(task.getValue());
        });
        task.setOnFailed((event) -> {
            messageProperty.unbind();
            handler.handle(null);
        });
        new Thread(task).start();
    }

    public void exportImage(Image image, File imageDirectory, String fileName) throws IOException {
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
        File imageFile = new File(imageDirectory, fileName + "_" + UUID.randomUUID() + "." + IMAGE_FILE_EXTENSION);
        if (imageFile.exists()) {
            throw new IOException("File already exists!");
        }
        ImageIO.write(renderedImage, IMAGE_FILE_EXTENSION, imageFile);
    }

    public boolean isImageDataValid(File choosenDirectory) {
        if (choosenDirectory == null || !choosenDirectory.exists() || !choosenDirectory.isDirectory()) {
            return false;
        }
        String[] fileNames = choosenDirectory.list();
        if (fileNames.length == 0) {
            return false;
        }
        // Check extension and file name
        for (String fileName : fileNames) {
            int indexOfNumberSeparator = fileName.indexOf("_");
            int indexOfExtension = fileName.lastIndexOf(".");
            if (indexOfNumberSeparator == -1 || indexOfExtension == -1) {
                return false;
            }
            String number = fileName.substring(0, indexOfNumberSeparator);
            try {
                Integer.parseInt(number);
            } catch (@SuppressWarnings("unused") NumberFormatException exception) {
                return false;
            }
            String extension = fileName.substring(indexOfExtension + 1);
            if (!IMAGE_FILE_EXTENSION.equalsIgnoreCase(extension)) {
                return false;
            }
        }
        return true;
    }

    public Map<Float[], Integer> loadImageLearningData(List<String> fileNames, double targetWidth) {
        Map<Float[], Integer> learningData = new HashMap<>();
        for (String fileName : fileNames) {
            try (InputStream inputStream = getClass().getResourceAsStream(IMAGE_DATA_FOLDER + fileName)) {
                createImageData(learningData, fileName, inputStream, targetWidth);
            } catch (@SuppressWarnings("unused") Exception e) {
                logger.log(Level.WARNING, "Could not load image data from embedded source!");
            }
        }
        return learningData;
    }

    private void createImageData(Map<Float[], Integer> learningData, String name, InputStream inputStream, double targetWidth) throws Exception {
        Integer imageNumber = Integer.parseInt(name.substring(0, name.indexOf("_")));
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        int imageWidth = bufferedImage.getWidth();
        double scaleFactor = targetWidth / imageWidth;
        WritableImage image = Utils.scaleImage(bufferedImage, scaleFactor);
        Float[] numberInputData = NetworkProcessor.getNumberInputData(image);
        learningData.put(numberInputData, imageNumber);
    }

    public Map<Float[], Integer> loadImageLearningData(File choosenDirectory, double targetWidth) {
        File[] files = choosenDirectory.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile();
            }
        });
        Map<Float[], Integer> learningData = new HashMap<>();
        for (File file : files) {
            try (InputStream inputStream = new FileInputStream(file)) {
                String fileName = file.getName();
                createImageData(learningData, fileName, inputStream, targetWidth);
            } catch (@SuppressWarnings("unused") Exception e) {
                logger.log(Level.WARNING, "Could not load image data from external source!");
            }
        }
        return learningData;
    }
}
