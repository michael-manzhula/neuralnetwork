package org.mmv.nn.ui.model;

import java.util.List;

import org.mmv.nn.network.Network;
import org.mmv.nn.ui.controller.XONetworkController;

public class GameData {

    public int cycleQuantity;
    public Network[] player1;
    public List<MoveData> player1Moves;
    public Network[] player2;
    public List<MoveData> player2Moves;
    public int[] gameField;

    public GameData(int cycleQuantity, Network[] player1, List<MoveData> player1Moves, Network[] player2, List<MoveData> player2Moves, int[] gameField) {
        super();
        this.cycleQuantity = cycleQuantity;
        this.player1 = player1;
        this.player1Moves = player1Moves;
        this.player2 = player2;
        this.player2Moves = player2Moves;
        this.gameField = gameField;
    }

    public Network[] getNetworkGroup(int cellType) {
        return XONetworkController.X_CELL == cellType ? player1 : player2;
    }

    public Network getNetwork(int cellType, int moveOrder) {
        Network[] group = getNetworkGroup(cellType);
        return group[moveOrder];
    }

    public List<MoveData> getMoves(int cellType) {
        return XONetworkController.X_CELL == cellType ? player1Moves : player2Moves;
    }

    public MoveData getMoveData(int cellType, int moveOrder) {
        List<MoveData> moveData = getMoves(cellType);
        for (MoveData data : moveData) {
            if (data.moveOrder == moveOrder) {
                return data;
            }
        }
        return null;
    }
}
