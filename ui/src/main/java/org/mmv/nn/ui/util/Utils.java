/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.util;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javafx.scene.transform.Transform;

public class Utils {

    private Utils() {
    }

    public static WritableImage scaleImage(Canvas canvas, double scaleFactor) {
        if (canvas == null) {
            return null;
        }
        int width = (int) (canvas.getWidth() * scaleFactor);
        int height = (int) (canvas.getHeight() * scaleFactor);
        WritableImage writableImage = new WritableImage(width, height);
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setTransform(Transform.scale(scaleFactor, scaleFactor));
        canvas.snapshot(snapshotParameters, writableImage);
        return writableImage;
    }

    public static WritableImage scaleImage(BufferedImage sourceImage, double scaleFactor) {
        int width = (int) (sourceImage.getWidth() * scaleFactor);
        int height = (int) (sourceImage.getHeight() * scaleFactor);
        BufferedImage targetImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.scale(scaleFactor, scaleFactor);
        AffineTransformOp scaleOp = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);
        targetImage = scaleOp.filter(sourceImage, targetImage);
        WritableImage image = new WritableImage(width, height);
        SwingFXUtils.toFXImage(targetImage, image);
        return image;
    }
}
