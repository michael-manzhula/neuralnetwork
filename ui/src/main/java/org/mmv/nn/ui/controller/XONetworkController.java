/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.result.Result;
import org.mmv.nn.ui.model.GameData;
import org.mmv.nn.ui.model.StatusData;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class XONetworkController extends BaseController implements Initializable, TabController {

    public static final int X_CELL = 1;
    public static final int O_CELL = -1;
    public static final int[][] LINES = new int[][] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, { 0, 4, 8 }, { 2, 4, 6 } };
    public static final int FREE_CELL = 0;
    public static final int GAME_FIELD_SIZE = 9;
    private static final int NETWORK_GROUP_SIZE = 5;
    private static final String USER_MARKER = "X";
    private static final String COMPUTER_MARKER = "O";

    @FXML
    private Button createButton;
    @FXML
    private Label statusLabel;
    @FXML
    private Button startGameButton;
    @FXML
    protected TextField layerNumberTextField;
    @FXML
    protected TextField learningRateTextField;
    @FXML
    private TextField cycleQuantityTextField;
    @FXML
    private Button internalGameButton;
    @FXML
    private Button disposeNetworkButton;
    @FXML
    private Button clearOutputButton;
    @FXML
    private TextArea outputTextArea;
    @FXML
    private Button button1;
    @FXML
    private Button button2;
    @FXML
    private Button button3;
    @FXML
    private Button button4;
    @FXML
    private Button button5;
    @FXML
    private Button button6;
    @FXML
    private Button button7;
    @FXML
    private Button button8;
    @FXML
    private Button button9;

    private Network[] networkGroup = new Network[5];
    private BooleanProperty gameFieldStatus;
    private List<int[]> moves;
    private Button[] gameButtons;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gameFieldStatus = new SimpleBooleanProperty(true);
        createButton.disableProperty().bind(interfaceStatus);
        statusLabel.disableProperty().bind(interfaceStatus);
        startGameButton.disableProperty().bind(interfaceStatus);
        layerNumberTextField.disableProperty().bind(interfaceStatus);
        learningRateTextField.disableProperty().bind(interfaceStatus);
        cycleQuantityTextField.disableProperty().bind(interfaceStatus);
        internalGameButton.disableProperty().bind(interfaceStatus);
        disposeNetworkButton.disableProperty().bind(interfaceStatus);
        clearOutputButton.disableProperty().bind(interfaceStatus);
        outputTextArea.disableProperty().bind(interfaceStatus);
        button1.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button2.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button3.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button4.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button5.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button6.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button7.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button8.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));
        button9.disableProperty().bind(Bindings.or(gameFieldStatus, interfaceStatus));

        button1.setOnAction((event) -> gameFieldButtonEventHandler(button1, 0));
        button2.setOnAction((event) -> gameFieldButtonEventHandler(button2, 1));
        button3.setOnAction((event) -> gameFieldButtonEventHandler(button3, 2));
        button4.setOnAction((event) -> gameFieldButtonEventHandler(button4, 3));
        button5.setOnAction((event) -> gameFieldButtonEventHandler(button5, 4));
        button6.setOnAction((event) -> gameFieldButtonEventHandler(button6, 5));
        button7.setOnAction((event) -> gameFieldButtonEventHandler(button7, 6));
        button8.setOnAction((event) -> gameFieldButtonEventHandler(button8, 7));
        button9.setOnAction((event) -> gameFieldButtonEventHandler(button9, 8));

        gameButtons = new Button[] { button1, button2, button3, button4, button5, button6, button7, button8, button9, };

        moves = new ArrayList<>();
    }

    @FXML
    private void createNetwork() {
        if (networkGroup[0] != null) {
            ButtonType answer = showDialog("Network is already created! Do you want to create a new network?", AlertType.CONFIRMATION);
            if (ButtonType.OK != answer) {
                return;
            }
        }
        float learningRate;
        int neuronLayersQuantity;
        try {
            learningRate = Float.parseFloat(learningRateTextField.getText());
            neuronLayersQuantity = Integer.parseInt(layerNumberTextField.getText());
        } catch (NumberFormatException ex) {
            String message = "Configuration is not correct! " + ex.getMessage();
            showDialog(message, AlertType.ERROR);
            return;
        }
        createNetworkGroup(networkGroup, 0, neuronLayersQuantity, learningRate);
    }

    private void createNetworkGroup(Network[] networkStorage, int index, int neuronLayersQuantity, float learningRate) {
        toggleUserInterface(false);
        NetworkEventHandler<Network> handler = (Network network) -> {
            if (network == null) {
                for (int i = 0; i < networkStorage.length; i++) {
                    networkStorage[i] = null;
                }
                showDialog("Error occured while creating network!", AlertType.ERROR);
                toggleUserInterface(true);
            } else {
                network.setId(index + 1);
                networkStorage[index] = network;
                int nextIndex = index + 1;
                if (nextIndex == NETWORK_GROUP_SIZE) {
                    statusLabel.setText("Created");
                    showDialog("Network is created!", AlertType.INFORMATION);
                    toggleUserInterface(true);
                } else {
                    createNetworkGroup(networkStorage, nextIndex, neuronLayersQuantity, learningRate);
                }
            }
        };
        StringProperty statusTextProperty = statusLabel.textProperty();
        networkProcessor.createNetwork(GAME_FIELD_SIZE, GAME_FIELD_SIZE, neuronLayersQuantity, learningRate, NetworkType.XO, statusTextProperty, handler);
    }

    @FXML
    private void startNewGame() {
        if (networkGroup[0] == null) {
            showDialog("Please create network!", AlertType.INFORMATION);
            return;
        }
        for (Button gameButton : gameButtons) {
            setGameButtonValue(gameButton, FREE_CELL);
        }
        moves = new ArrayList<>();
        gameFieldStatus.set(false);
    }

    @FXML
    private void startInternalGame() {
        // New group of networks will be created, game will be played between
        // two groups that will learn during playing.
        if (networkGroup[0] == null) {
            showDialog("Please create network!", AlertType.INFORMATION);
            return;
        }
        String cycleQuantityString = cycleQuantityTextField.getText();
        try {
            Integer.parseInt(cycleQuantityString);
        } catch (NumberFormatException e) {
            showDialog("The quantity of cycles is not correct! " + e.getMessage(), AlertType.ERROR);
            return;
        }
        int neuronLayersQuantity = networkGroup[0].getHiddenLayersNumber();
        float learningRate = networkGroup[0].getLearningRate();
        Network[] networkStorage = new Network[NETWORK_GROUP_SIZE];
        createOpponentNetworkGroup(0, networkStorage, neuronLayersQuantity, learningRate);
    }

    private void createOpponentNetworkGroup(int index, Network[] networkStorage, int neuronLayersQuantity, float learningRate) {
        toggleUserInterface(false);
        NetworkEventHandler<Network> handler = (Network network) -> {
            if (network == null) {
                for (int i = 0; i < networkStorage.length; i++) {
                    networkStorage[i] = null;
                }
                showDialog("Error occured while creating network!", AlertType.ERROR);
                toggleUserInterface(true);
            } else {
                networkStorage[index] = network;
                int nextIndex = index + 1;
                if (nextIndex == NETWORK_GROUP_SIZE) {
                    startLearning(networkStorage);
                } else {
                    createOpponentNetworkGroup(nextIndex, networkStorage, neuronLayersQuantity, learningRate);
                }
            }
        };
        StringProperty statusTextProperty = statusLabel.textProperty();
        networkProcessor.createNetwork(GAME_FIELD_SIZE, GAME_FIELD_SIZE, neuronLayersQuantity, learningRate, NetworkType.XO, statusTextProperty, handler);
    }

    private void startLearning(Network[] opponentNetworkGroup) {
        // Opponent network group will play 'X' imitating user
        Alert cancelAlert = createCancelAlert();
        StringProperty statusTextProperty = cancelAlert.contentTextProperty();
        toggleUserInterface(false);
        int cycleQuantity = Integer.parseInt(cycleQuantityTextField.getText());
        int[] gameField = new int[GAME_FIELD_SIZE];
        GameData gameData = new GameData(cycleQuantity, opponentNetworkGroup, new ArrayList<>(), networkGroup, new ArrayList<>(), gameField);
        NetworkEventHandler<StatusData> handler = new NetworkEventHandler<StatusData>() {
            @Override
            public void handle(StatusData status) {
                if (status.isError) {
                    cancelAlert.close();
                    showDialog("Error occured during internal game...", AlertType.ERROR);
                    toggleUserInterface(true);
                } else if (status.isFinished) {
                    cancelAlert.close();
                    showDialog("Learning finished!", AlertType.INFORMATION);
                    toggleUserInterface(true);
                }
            }
        };
        networkProcessor.startInternalXOGame(gameData, handler, cancelRequest, statusTextProperty);
        cancelRequest.set(false);
        cancelAlert.setOnCloseRequest((event) -> cancelRequest.set(true));
        cancelAlert.show();
    }

    @FXML
    private void disposeNetwork() {
        ButtonType answer = showDialog("Are you sure?", AlertType.CONFIRMATION);
        if (ButtonType.OK == answer) {
            for (int i = 0; i < networkGroup.length; i++) {
                networkGroup[i] = null;
            }
            System.gc();
            statusLabel.setText("Disposed");
        }
    }

    @FXML
    private void clearOutput() {
        outputTextArea.setText("");
    }

    private void gameFieldButtonEventHandler(Button targetButton, int fieldNumber) {
        gameFieldStatus.set(true);
        setGameButtonValue(targetButton, X_CELL);
        makeMove(fieldNumber, X_CELL, networkGroup);
    }

    private void makeMove(int fieldNumber, int cellType, Network[] networks) {
        if (moves.isEmpty()) {
            // First move
            int[] gameField = new int[GAME_FIELD_SIZE];
            for (int i = 0; i < gameField.length; i++) {
                gameField[i] = FREE_CELL;
            }
            moves.add(gameField);
        }
        final int[] previousGameFileld = moves.get(moves.size() - 1);
        previousGameFileld[fieldNumber] = cellType;
        if (isGameOver(moves.get(moves.size() - 1), true)) {
            return;
        }
        Network targetNetwork = networks[moves.size() - 1];
        NetworkEventHandler<Result<Float>[]> handler = new NetworkEventHandler<Result<Float>[]>() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void handle(Result<Float>[] results) {
                if (results == null) {
                    showDialog("Error occured while processing data!", AlertType.ERROR);
                    moves.clear();
                } else {
                    handleResults(results, previousGameFileld);
                }
                gameFieldStatus.set(false);
            }
        };
        Float[] inputData = new Float[previousGameFileld.length];
        for (int i = 0; i < previousGameFileld.length; i++) {
            inputData[i] = (float) previousGameFileld[i];
        }
        targetNetwork.analyze(inputData, handler);
    }

    private void handleResults(Result<Float>[] results, int[] previousGameFileld) {
        int cellIndex = networkProcessor.getCellIndex(previousGameFileld, results);
        int[] gameField = new int[previousGameFileld.length];
        for (int i = 0; i < previousGameFileld.length; i++) {
            gameField[i] = previousGameFileld[i];
        }
        gameField[cellIndex] = O_CELL;
        moves.add(gameField);
        Button targetButton = gameButtons[cellIndex];
        setGameButtonValue(targetButton, O_CELL);
        isGameOver(moves.get(moves.size() - 1), false);
    }

    public boolean isGameOver(int[] gameField, boolean isUser) {
        Boolean gameStatus = networkProcessor.checkGameStatus(gameField);
        if (gameStatus == null) {
            showDialog("Draw...", AlertType.INFORMATION);
            moves.clear();
            return true;
        }
        if (gameStatus) {
            String message;
            if (isUser) {
                message = "You won!";
            } else {
                message = "You lose...";
            }
            showDialog(message, AlertType.INFORMATION);
            moves.clear();
            return true;
        }
        return false;
    }

    private void setGameButtonValue(Button button, int value) {
        switch (value) {
            case X_CELL:
                button.setText(USER_MARKER);
                button.setTextFill(Color.RED);
                button.disableProperty().unbind();
                button.setDisable(true);
                break;
            case O_CELL:
                button.setText(COMPUTER_MARKER);
                button.setTextFill(Color.BLUE);
                button.disableProperty().unbind();
                button.setDisable(true);
                break;
            case FREE_CELL:
                button.setText("");
                button.disableProperty().bind(gameFieldStatus);
                break;
            default:
                showDialog("Unknown cell code!", AlertType.ERROR);
                break;
        }
    }

    @Override
    public void setNetwork(Network[] network) {
        showNotImplemented();
    }

    @Override
    public void saveNetwork() {
        showNotImplemented();
    }

    private void showNotImplemented() {
        showDialog("Not implemented!", AlertType.WARNING);
    }
}
