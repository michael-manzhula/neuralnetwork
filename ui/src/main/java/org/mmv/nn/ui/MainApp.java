/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui;

import org.mmv.nn.ui.controller.SceneController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static final String APP_NAME = "Neural network";
    public static final String APP_VERSION = " v1.0";
    private static final String DEBUG_ARGUMENT = "debug";
    
    public static boolean DEBUG_MODE;

    public static void main(String[] args) {
        for (String arg : args) {
            if (MainApp.DEBUG_ARGUMENT.equalsIgnoreCase(arg)) {
                MainApp.DEBUG_MODE = true;
                System.out.println("Debug mode is ON");
            }
        }
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/scene.fxml"));
        Parent root = loader.load();
        SceneController controller = loader.getController();
        controller.setStage(stage);

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle(APP_NAME + APP_VERSION);
        stage.setScene(scene);
        stage.show();
    }
}
