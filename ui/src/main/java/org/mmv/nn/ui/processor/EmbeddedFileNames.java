/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.processor;

import java.util.Arrays;
import java.util.List;

public class EmbeddedFileNames {
    public static final String n1 = "0_21106da4-2511-408f-a607-b3ea45829d36.png";
    public static final String n2 = "0_cbdea986-1f05-45bc-8c9e-89af961ae895.png";
    public static final String n3 = "1_6c9dacc9-89ea-415d-970b-7992d084ceef.png";
    public static final String n4 = "1_e297dbc5-af04-4818-ba8c-4f94b90fe1a8.png";
    public static final String n5 = "2_4836caa9-500b-405a-9f19-20cd6244a01b.png";
    public static final String n6 = "2_eda13df5-fdf6-4bcd-822e-63fc66a1e2fc.png";
    public static final String n7 = "3_43004bf2-9f28-47f8-8b44-f57fcbdf287c.png";
    public static final String n8 = "3_e97381e3-fd5d-45f7-9570-20a72cb8f3c8.png";
    public static final String n9 = "4_21e873f1-7b1f-4ed0-b392-f8c91b6c5019.png";
    public static final String n10 = "4_5714dc37-21cb-47e5-80a4-19fb2cae7be1.png";
    public static final String n11 = "4_731973e6-b701-47cf-9b0b-19fb6f187ac2.png";
    public static final String n12 = "5_0496803a-0f87-4b4c-a2cd-fea5344ee132.png";
    public static final String n13 = "5_11c4b06d-afdf-43ba-82d1-6c1987f87834.png";
    public static final String n14 = "6_f28a3a58-3eb3-41b9-8e8e-aef068436601.png";
    public static final String n15 = "6_f3580126-201b-4bc3-b9cb-efc4293c70d6.png";
    public static final String n16 = "7_01cdc250-6ad4-4fd2-9349-c10880e1dae1.png";
    public static final String n17 = "7_63ca693f-1bca-448b-8e3d-7988a69e3f70.png";
    public static final String n18 = "8_e6db8da0-52f2-416b-90ec-c831e13f4c71.png";
    public static final String n19 = "8_fc24395d-fb59-4ed3-aa83-f1c3c89333f6.png";
    public static final String n20 = "9_93fa634d-4b9d-461a-bb17-1a3fc0854289.png";
    public static final String n21 = "9_d3a6de89-4666-4549-a402-b40fab5d663b.png";

    public static List<String> getEmbeddedFileNames() {
        return Arrays.asList(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21);
    }
}
