/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.ui.MainApp;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class SceneController extends BaseController implements Initializable {

    private static final String OPEN_NETWORK_TITLE = "Open neural network";

    @FXML
    private MenuBar menuBar;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab binaryNetworkTab;
    @FXML
    private Tab numberNetworkTab;
    @FXML
    private Tab xoNetworkTab;

    private BinaryNetworkController binaryNetworkController;
    private NumberNetworkController numberNetworkController;
    private XONetworkController xoNetworkController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/binary_network_tab.fxml"));
            AnchorPane pane = loader.load();
            binaryNetworkController = loader.getController();
            binaryNetworkTab.setContent(pane);

            loader = new FXMLLoader(getClass().getResource("/fxml/number_network_tab.fxml"));
            pane = loader.load();
            numberNetworkController = loader.getController();
            numberNetworkTab.setContent(pane);

            loader = new FXMLLoader(getClass().getResource("/fxml/xo_network_tab.fxml"));
            pane = loader.load();
            xoNetworkController = loader.getController();
            xoNetworkTab.setContent(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }
        menuBar.disableProperty().bind(interfaceStatus);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void loadNetworkMenu() {
        ButtonType answer = showDialog("It can take long time... Do you want to continue?", AlertType.CONFIRMATION);
        if (ButtonType.OK != answer) {
            return;
        }
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(OPEN_NETWORK_TITLE);
        if (previousLocation != null) {
            directoryChooser.setInitialDirectory(previousLocation);
        }
        File networkDirectory = directoryChooser.showDialog(stage);
        if (networkDirectory == null) {
            return;
        }
        previousLocation = networkDirectory;
        String networkPath = networkDirectory.getAbsolutePath();
        Alert messageDialog = showMessageDialog();
        NetworkEventHandler<Network[]> handler = new NetworkEventHandler<Network[]>() {
            @SuppressWarnings("synthetic-access")
            @Override
            public void handle(Network[] networks) {
                closeAlert(messageDialog);
                if (networks == null || networks.length == 0) {
                    showDialog("Network could not be loaded from provided file!", AlertType.ERROR);
                } else {
                    Network network = networks[0];
                    if (NetworkType.BINARY == network.getNetworkType()) {
                        binaryNetworkController.setNetwork(networks);
                        String layerNumber = String.valueOf(network.getHiddenLayersNumber());
                        binaryNetworkController.binaryLayersTextField.setText(layerNumber);
                        String learningRate = String.valueOf(network.getLearningRate());
                        binaryNetworkController.binaryLearningRateTextField.setText(learningRate);
                        showDialog("Network is loaded!", AlertType.INFORMATION);
                    } else if (NetworkType.NUMBER == network.getNetworkType()) {
                        numberNetworkController.setNetwork(networks);
                        String layerNumber = String.valueOf(network.getHiddenLayersNumber());
                        numberNetworkController.numberNetworkLayersTextField.setText(layerNumber);
                        String learningRate = String.valueOf(network.getLearningRate());
                        numberNetworkController.numberNetworkLearningRateTextField.setText(learningRate);
                        showDialog("Network is loaded!", AlertType.INFORMATION);
                    } else if (NetworkType.XO == network.getNetworkType()) {
                        xoNetworkController.setNetwork(networks);
                        String layerNumber = String.valueOf(network.getHiddenLayersNumber());
                        xoNetworkController.layerNumberTextField.setText(layerNumber);
                        String learningRate = String.valueOf(network.getLearningRate());
                        xoNetworkController.learningRateTextField.setText(learningRate);
                        showDialog("Network is loaded!", AlertType.INFORMATION);
                    } else {
                        showDialog("Type of network is unknown!", AlertType.WARNING);
                    }
                }
                toggleUserInterface(true);
            }
        };
        toggleUserInterface(false);
        importExportProcessor.importNetwork(handler, networkPath, messageDialog.contentTextProperty());
    }

    @FXML
    private void saveAsNetworkMenu() {
        ObservableList<Tab> tabs = tabPane.getTabs();
        int index = -1;
        for (Tab tab : tabs) {
            if (tab.isSelected()) {
                index = tabs.indexOf(tab);
                break;
            }
        }
        switch (index) {
            case 0:
                // Binary network
                binaryNetworkController.saveNetwork();
                break;
            case 1:
                // Number network
                numberNetworkController.saveNetwork();
                break;
            case 2:
                // XO network
                xoNetworkController.saveNetwork();
                break;
            default:
                showDialog("Cant't find network...", AlertType.INFORMATION);
                break;
        }
    }

    @FXML
    private void quitMenu() {
        ButtonType answer = showDialog("Do you really want to quit?", AlertType.CONFIRMATION);
        if (ButtonType.OK == answer) {
            System.exit(0);
        }
    }

    @FXML
    private void aboutMenu() {
        showDialog(MainApp.APP_NAME + MainApp.APP_VERSION + "\nmichael.manzhula@gmail.com", AlertType.INFORMATION);
    }

    @Override
    protected void toggleUserInterface(boolean isEnabled) {
        super.toggleUserInterface(isEnabled);
        binaryNetworkController.toggleUserInterface(isEnabled);
        numberNetworkController.toggleUserInterface(isEnabled);
        xoNetworkController.toggleUserInterface(isEnabled);
    }
}
