/*
 * Copyright (C) 2018 michael.manzhula@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.mmv.nn.ui.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mmv.nn.network.Network;
import org.mmv.nn.network.NetworkType;
import org.mmv.nn.network.node.NetworkEventHandler;
import org.mmv.nn.network.result.Result;
import org.mmv.nn.ui.processor.EmbeddedFileNames;
import org.mmv.nn.ui.processor.NetworkProcessor;
import org.mmv.nn.ui.util.Utils;

import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.DirectoryChooser;

public class NumberNetworkController extends BaseController implements Initializable, TabController {

    private static final String IMAGE_EXPORT_DIRECTORY_TITLE = "Choose directory";
    private static final double CANVAS_CURSOR_DIAMETER = 20d;
    private static final String RESOLUTION_200 = "200 x 200";
    private static final String RESOLUTION_100 = "100 x 100";
    private static final String RESOLUTION_50 = "50 x 50";
    private static final String RESOLUTION_40 = "40 x 40";

    @FXML
    private Canvas canvas;
    @FXML
    private Button cleanCanvasButton;
    @FXML
    private Button canvasColorButton;
    @FXML
    private Rectangle colorRectangle;
    @FXML
    private Button numberNetworkCreateButton;
    @FXML
    private Button numberNetworkEmitButton;
    @FXML
    private Button numberNetworkTeachByExternalDataButton;
    @FXML
    private Button numberNetworkTeachByEmbeddedDataButton;
    @FXML
    private Button numberNetworkDisposeButton;
    @FXML
    private Button numberNetworkClearOutputButton;
    @FXML
    protected TextField numberNetworkLayersTextField;
    @FXML
    protected TextField numberNetworkLearningRateTextField;
    @FXML
    private TextField numberNetworkLearningCyclesTextField;
    @FXML
    private Label numberNetworkStatusLabel;
    @FXML
    private TextArea numberNetworkOutputArea;
    @FXML
    private Button exportImageButton;
    @FXML
    private Button chooseExportDirectoryButton;
    @FXML
    private ChoiceBox<String> resolutionChoiseBox;
    private double imageScaleFactor;

    private Color currentColor;
    private Network numberNetwork;
    private File imageDirectory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initCanvas();
        cleanCanvasButton.disableProperty().bind(interfaceStatus);
        canvasColorButton.disableProperty().bind(interfaceStatus);
        currentColor = Color.BLACK;
        colorRectangle.setFill(currentColor);
        numberNetworkCreateButton.disableProperty().bind(interfaceStatus);
        numberNetworkEmitButton.disableProperty().bind(interfaceStatus);
        numberNetworkTeachByExternalDataButton.disableProperty().bind(interfaceStatus);
        numberNetworkTeachByEmbeddedDataButton.disableProperty().bind(interfaceStatus);
        numberNetworkDisposeButton.disableProperty().bind(interfaceStatus);
        numberNetworkClearOutputButton.disableProperty().bind(interfaceStatus);
        numberNetworkLayersTextField.disableProperty().bind(interfaceStatus);
        numberNetworkLearningRateTextField.disableProperty().bind(interfaceStatus);
        numberNetworkLearningCyclesTextField.disableProperty().bind(interfaceStatus);
        exportImageButton.disableProperty().bind(interfaceStatus);
        chooseExportDirectoryButton.disableProperty().bind(interfaceStatus);
        resolutionChoiseBox.disableProperty().bind(interfaceStatus);
        resolutionChoiseBox.getItems().addAll(RESOLUTION_200, RESOLUTION_100, RESOLUTION_50, RESOLUTION_40);
        resolutionChoiseBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(@SuppressWarnings("rawtypes") ObservableValue ov, String v, String nv) {
                setImageScaleFactor(nv);
            }
        });
        resolutionChoiseBox.getSelectionModel().selectLast();
    }

    @FXML
    private void exportImage() {
        try {
            String fileName = showPromptDialog("File name: ");
            if (!isImageNameValid(fileName)) {
                showDialog("File name must be a number from 0 to 9!", AlertType.WARNING);
                return;
            }
            if (imageDirectory == null) {
                chooseDefaultLocation();
            }
            int width = (int) canvas.getWidth();
            int height = (int) canvas.getHeight();
            WritableImage image = new WritableImage(width, height);
            canvas.snapshot(null, image);
            importExportProcessor.exportImage(image, imageDirectory, fileName);
        } catch (IOException ex) {
            Logger.getLogger(SceneController.class.getName()).log(Level.SEVERE, null, ex);
            showDialog("Error occured while saving image! " + ex.getMessage(), AlertType.ERROR);
        }
    }

    @FXML
    private void chooseExportDirectory() {
        chooseDefaultLocation();
    }

    private void chooseDefaultLocation() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(IMAGE_EXPORT_DIRECTORY_TITLE);
        if (imageDirectory != null) {
            directoryChooser.setInitialDirectory(imageDirectory);
        }
        File choosenDirectory = directoryChooser.showDialog(stage);
        if (choosenDirectory != null) {
            imageDirectory = choosenDirectory;
        }
    }

    @FXML
    private void numberNetworkCreate() {
        if (numberNetwork != null) {
            ButtonType answer = showDialog("Network is already created! Do you want to create a new network?", AlertType.CONFIRMATION);
            if (ButtonType.OK != answer) {
                return;
            }
        }
        float learningRate;
        int neuronLayersQuantity;
        try {
            learningRate = Float.parseFloat(numberNetworkLearningRateTextField.getText());
            neuronLayersQuantity = Integer.parseInt(numberNetworkLayersTextField.getText());
        } catch (NumberFormatException ex) {
            String message = "Configuration is not correct! " + ex.getMessage();
            showDialog(message, AlertType.ERROR);
            return;
        }
        toggleUserInterface(false);
        NetworkEventHandler<Network> handler = (Network network) -> {
            if (network == null) {
                showDialog("Error occured while creating network!", AlertType.ERROR);
            } else {
                network.setId(1);
                numberNetwork = network;
                numberNetworkStatusLabel.setText("Created");
                showDialog("Number network is created!", AlertType.INFORMATION);
            }
            toggleUserInterface(true);
        };
        StringProperty statusTextProperty = numberNetworkStatusLabel.textProperty();
        int inputNumber = (int) (canvas.getWidth() * canvas.getHeight() * Math.pow(imageScaleFactor, 2));
        networkProcessor.createNetwork(inputNumber, 10, neuronLayersQuantity, learningRate, NetworkType.NUMBER, statusTextProperty, handler);
    }

    @FXML
    private void numberNetworkEmit() {
        if (numberNetwork == null) {
            showDialog("Number network is not created!", AlertType.WARNING);
            return;
        }
        // Get image from canvas
        toggleUserInterface(false);
        WritableImage writableImage = Utils.scaleImage(canvas, imageScaleFactor);
        Float[] inputData = NetworkProcessor.getNumberInputData(writableImage);
        if (inputData == null) {
            toggleUserInterface(true);
            showDialog("Error occured during converting image to pixels!", AlertType.ERROR);
            return;
        }
        NetworkEventHandler<Result<Float>[]> handler = new NetworkEventHandler<Result<Float>[]>() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void handle(Result<Float>[] results) {
                if (results == null) {
                    showDialog("Error occured while processing data!", AlertType.ERROR);
                } else {
                    showNumberNetworkResult(results);
                }
                toggleUserInterface(true);
            }
        };
        toggleUserInterface(false);
        numberNetwork.analyze(inputData, handler);
    }

    private void showNumberNetworkResult(Result<Float>[] results) {
        StringBuilder builder = new StringBuilder();
        for (Result<Float> result : results) {
            builder.append(result.getOrder() + " (" + result.getNumberProbability() + "); ");
        }
        Arrays.sort(results);
        Result<Float> bestResult = results[0];
        builder.append("\nCorrect result is: " + bestResult.getOrder() + " with probability " + bestResult.getNumberProbability() + "\n\n");
        numberNetworkOutputArea.insertText(0, builder.toString());
    }

    @FXML
    private void teachNumberNetworkByEmbeddedData() {
        teachNumberNetwork(false);
    }

    @FXML
    private void teachNumberNetworkByExternalData() {
        teachNumberNetwork(true);
    }

    private void teachNumberNetwork(boolean isExternalData) {
        if (numberNetwork == null) {
            showDialog("Network is not created!", AlertType.WARNING);
            return;
        }
        String cycleQuantityString = numberNetworkLearningCyclesTextField.getText();
        int cycleQuantity;
        try {
            cycleQuantity = Integer.parseInt(cycleQuantityString);
        } catch (NumberFormatException e) {
            showDialog("The quantity of cycles is not correct! " + e.getMessage(), AlertType.ERROR);
            return;
        }
        Map<Float[], Integer> learningData;
        File choosenDirectory;
        int inputNumber = numberNetwork.getInputNumber();
        double imageWidth = Math.sqrt(inputNumber);
        if (isExternalData) {
            // Load learning data from external source
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle(IMAGE_EXPORT_DIRECTORY_TITLE);
            if (imageDirectory != null) {
                directoryChooser.setInitialDirectory(imageDirectory);
            }
            choosenDirectory = directoryChooser.showDialog(stage);
            if (choosenDirectory == null) {
                return;
            }
            if (!importExportProcessor.isImageDataValid(choosenDirectory)) {
                showDialog("Image data is not valid!", AlertType.WARNING);
                return;
            }
            learningData = importExportProcessor.loadImageLearningData(choosenDirectory, imageWidth);
        } else {
            // Load learning data from embedded source
            List<String> embeddedFileNames = EmbeddedFileNames.getEmbeddedFileNames();
            learningData = importExportProcessor.loadImageLearningData(embeddedFileNames, imageWidth);
        }
        ButtonType answer = showDialog("It can take long time... Do you want to continue?", AlertType.CONFIRMATION);
        if (ButtonType.OK != answer) {
            return;
        }
        Alert cancelAlert = createCancelAlert();
        StringProperty statusTextProperty = cancelAlert.contentTextProperty();
        NetworkEventHandler<Boolean> handler = (Boolean result) -> {
            numberNetworkStatusLabel.setText("Ok");
            toggleUserInterface(true);
            if (cancelAlert.isShowing()) {
                cancelAlert.close();
            }
            if (result) {
                showDialog("Learning finished!", AlertType.INFORMATION);
            } else {
                showDialog("Error occured during learning!", AlertType.ERROR);
            }
            toggleUserInterface(true);
        };
        toggleUserInterface(false);
        cancelRequest.set(false);
        networkProcessor.teachNumberNetwork(numberNetwork, cycleQuantity, learningData, statusTextProperty, handler, cancelRequest);
        cancelAlert.setOnCloseRequest((event) -> cancelRequest.set(true));
        cancelAlert.show();
    }

    @FXML
    private void clearNumberNetworkOutput() {
        numberNetworkOutputArea.setText("");
    }

    @FXML
    private void disposeNumberNetwork() {
        numberNetwork = null;
        System.gc();
    }

    @FXML
    private void cleanCanvas() {
        setCanvasColor(Color.WHITE);
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        setCanvasColor(Color.BLACK);
    }

    private void setCanvasColor(Color color) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(color);
        context.setStroke(color);
    }

    @FXML
    private void canvasMouseDragged(MouseEvent event) {
        boolean primaryButtonDown = event.isPrimaryButtonDown();
        if (!primaryButtonDown) {
            return;
        }
        double x = event.getX() - CANVAS_CURSOR_DIAMETER / 2;
        double y = event.getY() - CANVAS_CURSOR_DIAMETER / 2;
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.fillOval(x, y, CANVAS_CURSOR_DIAMETER, CANVAS_CURSOR_DIAMETER);
    }

    @FXML
    private void changeColor() {
        if (Color.BLACK.equals(currentColor)) {
            currentColor = Color.WHITE;
        } else {
            currentColor = Color.BLACK;
        }
        colorRectangle.setFill(currentColor);
        setCanvasColor(currentColor);
    }

    private boolean isImageNameValid(String imageName) {
        if (imageName == null || imageName.length() == 0) {
            return false;
        }
        try {
            int number = Integer.parseInt(imageName);
            if (number < 0 || number > 10) {
                throw new IllegalArgumentException("Number is not in range 0 - 9!");
            }
        } catch (@SuppressWarnings("unused") NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private void initCanvas() {
        canvas.disableProperty().bind(interfaceStatus);
        cleanCanvas();
    }

    public void setImageScaleFactor(String resolution) {
        switch (resolution) {
            case RESOLUTION_200:
                imageScaleFactor = 1;
                break;
            case RESOLUTION_100:
                imageScaleFactor = 0.5;
                break;
            case RESOLUTION_50:
                imageScaleFactor = 0.25;
                break;
            case RESOLUTION_40:
                imageScaleFactor = 0.2;
                break;
            default:
                imageScaleFactor = 0.2;
                break;
        }
    }

    @Override
    public void setNetwork(Network[] network) {
        numberNetwork = network[0];
        numberNetworkStatusLabel.setText("Loaded");
    }

    @Override
    public void saveNetwork() {
        super.saveNetwork(new Network[] { numberNetwork });
    }
}
