package org.mmv.nn.ui.model;

public class StatusData {

    public boolean isFinished;
    public boolean isError;

    public StatusData(boolean isFinished, boolean isError) {
        this.isFinished = isFinished;
        this.isError = isError;
    }
}
